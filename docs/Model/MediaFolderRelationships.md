# MediaFolderRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**\Swagger\Client\Model\MediaFolderRelationshipsParent**](MediaFolderRelationshipsParent.md) |  | [optional] 
**children** | [**\Swagger\Client\Model\MediaFolderRelationshipsChildren**](MediaFolderRelationshipsChildren.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\MediaFolderRelationshipsMedia**](MediaFolderRelationshipsMedia.md) |  | [optional] 
**default_folder** | [**\Swagger\Client\Model\MediaFolderRelationshipsDefaultFolder**](MediaFolderRelationshipsDefaultFolder.md) |  | [optional] 
**configuration** | [**\Swagger\Client\Model\MediaFolderRelationshipsConfiguration**](MediaFolderRelationshipsConfiguration.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

