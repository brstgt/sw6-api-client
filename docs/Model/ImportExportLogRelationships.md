# ImportExportLogRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**\Swagger\Client\Model\ImportExportLogRelationshipsUser**](ImportExportLogRelationshipsUser.md) |  | [optional] 
**profile** | [**\Swagger\Client\Model\ImportExportLogRelationshipsProfile**](ImportExportLogRelationshipsProfile.md) |  | [optional] 
**file** | [**\Swagger\Client\Model\ImportExportLogRelationshipsFile**](ImportExportLogRelationshipsFile.md) |  | [optional] 
**invalid_records_log** | [**\Swagger\Client\Model\ImportExportLogRelationshipsInvalidRecordsLog**](ImportExportLogRelationshipsInvalidRecordsLog.md) |  | [optional] 
**failed_import_log** | [**\Swagger\Client\Model\ImportExportLogRelationshipsFailedImportLog**](ImportExportLogRelationshipsFailedImportLog.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

