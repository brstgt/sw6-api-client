# DocumentBaseConfigSalesChannelRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | [**\Swagger\Client\Model\DocumentBaseConfigSalesChannelRelationshipsDocumentType**](DocumentBaseConfigSalesChannelRelationshipsDocumentType.md) |  | [optional] 
**document_base_config** | [**\Swagger\Client\Model\DocumentBaseConfigSalesChannelRelationshipsDocumentBaseConfig**](DocumentBaseConfigSalesChannelRelationshipsDocumentBaseConfig.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\DocumentBaseConfigSalesChannelRelationshipsSalesChannel**](DocumentBaseConfigSalesChannelRelationshipsSalesChannel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

