# MediaFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**user_id** | **string** |  | [optional] 
**media_folder_id** | **string** |  | [optional] 
**mime_type** | **string** |  | [optional] 
**file_extension** | **string** |  | [optional] 
**uploaded_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**file_name** | **string** |  | [optional] 
**file_size** | **int** |  | [optional] 
**meta_data** | **object** |  | [optional] 
**media_type** | **object** |  | [optional] 
**alt** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**has_file** | **bool** |  | [optional] 
**private** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**tags** | [**\Swagger\Client\Model\TagFlat**](TagFlat.md) |  | [optional] 
**thumbnails** | [**\Swagger\Client\Model\MediaThumbnailFlat**](MediaThumbnailFlat.md) |  | [optional] 
**user** | [**\Swagger\Client\Model\UserFlat**](UserFlat.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\CategoryFlat**](CategoryFlat.md) |  | [optional] 
**product_manufacturers** | [**\Swagger\Client\Model\ProductManufacturerFlat**](ProductManufacturerFlat.md) |  | [optional] 
**product_media** | [**\Swagger\Client\Model\ProductMediaFlat**](ProductMediaFlat.md) |  | [optional] 
**avatar_user** | [**\Swagger\Client\Model\UserFlat**](UserFlat.md) |  | [optional] 
**media_folder** | [**\Swagger\Client\Model\MediaFolderFlat**](MediaFolderFlat.md) |  | [optional] 
**property_group_options** | [**\Swagger\Client\Model\PropertyGroupOptionFlat**](PropertyGroupOptionFlat.md) |  | [optional] 
**mail_template_media** | [**\Swagger\Client\Model\MailTemplateMediaFlat**](MailTemplateMediaFlat.md) |  | [optional] 
**document_base_configs** | [**\Swagger\Client\Model\DocumentBaseConfigFlat**](DocumentBaseConfigFlat.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\ShippingMethodFlat**](ShippingMethodFlat.md) |  | [optional] 
**payment_methods** | [**\Swagger\Client\Model\PaymentMethodFlat**](PaymentMethodFlat.md) |  | [optional] 
**product_configurator_settings** | [**\Swagger\Client\Model\ProductConfiguratorSettingFlat**](ProductConfiguratorSettingFlat.md) |  | [optional] 
**order_line_items** | [**\Swagger\Client\Model\OrderLineItemFlat**](OrderLineItemFlat.md) |  | [optional] 
**cms_blocks** | [**\Swagger\Client\Model\CmsBlockFlat**](CmsBlockFlat.md) |  | [optional] 
**cms_sections** | [**\Swagger\Client\Model\CmsSectionFlat**](CmsSectionFlat.md) |  | [optional] 
**cms_pages** | [**\Swagger\Client\Model\CmsPageFlat**](CmsPageFlat.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\DocumentFlat**](DocumentFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

