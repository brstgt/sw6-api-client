# SalesChannelRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currencies** | [**\Swagger\Client\Model\SalesChannelRelationshipsCurrencies**](SalesChannelRelationshipsCurrencies.md) |  | [optional] 
**languages** | [**\Swagger\Client\Model\SalesChannelRelationshipsLanguages**](SalesChannelRelationshipsLanguages.md) |  | [optional] 
**countries** | [**\Swagger\Client\Model\SalesChannelRelationshipsCountries**](SalesChannelRelationshipsCountries.md) |  | [optional] 
**payment_methods** | [**\Swagger\Client\Model\SalesChannelRelationshipsPaymentMethods**](SalesChannelRelationshipsPaymentMethods.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\SalesChannelRelationshipsShippingMethods**](SalesChannelRelationshipsShippingMethods.md) |  | [optional] 
**type** | [**\Swagger\Client\Model\SalesChannelRelationshipsType**](SalesChannelRelationshipsType.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\SalesChannelRelationshipsLanguage**](SalesChannelRelationshipsLanguage.md) |  | [optional] 
**customer_group** | [**\Swagger\Client\Model\SalesChannelRelationshipsCustomerGroup**](SalesChannelRelationshipsCustomerGroup.md) |  | [optional] 
**currency** | [**\Swagger\Client\Model\SalesChannelRelationshipsCurrency**](SalesChannelRelationshipsCurrency.md) |  | [optional] 
**payment_method** | [**\Swagger\Client\Model\SalesChannelRelationshipsPaymentMethod**](SalesChannelRelationshipsPaymentMethod.md) |  | [optional] 
**shipping_method** | [**\Swagger\Client\Model\SalesChannelRelationshipsShippingMethod**](SalesChannelRelationshipsShippingMethod.md) |  | [optional] 
**country** | [**\Swagger\Client\Model\SalesChannelRelationshipsCountry**](SalesChannelRelationshipsCountry.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\SalesChannelRelationshipsOrders**](SalesChannelRelationshipsOrders.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\SalesChannelRelationshipsCustomers**](SalesChannelRelationshipsCustomers.md) |  | [optional] 
**domains** | [**\Swagger\Client\Model\SalesChannelRelationshipsDomains**](SalesChannelRelationshipsDomains.md) |  | [optional] 
**system_configs** | [**\Swagger\Client\Model\SalesChannelRelationshipsSystemConfigs**](SalesChannelRelationshipsSystemConfigs.md) |  | [optional] 
**navigation_category** | [**\Swagger\Client\Model\SalesChannelRelationshipsNavigationCategory**](SalesChannelRelationshipsNavigationCategory.md) |  | [optional] 
**footer_category** | [**\Swagger\Client\Model\SalesChannelRelationshipsFooterCategory**](SalesChannelRelationshipsFooterCategory.md) |  | [optional] 
**service_category** | [**\Swagger\Client\Model\SalesChannelRelationshipsServiceCategory**](SalesChannelRelationshipsServiceCategory.md) |  | [optional] 
**product_visibilities** | [**\Swagger\Client\Model\SalesChannelRelationshipsProductVisibilities**](SalesChannelRelationshipsProductVisibilities.md) |  | [optional] 
**hreflang_default_domain** | [**\Swagger\Client\Model\SalesChannelRelationshipsHreflangDefaultDomain**](SalesChannelRelationshipsHreflangDefaultDomain.md) |  | [optional] 
**mail_header_footer** | [**\Swagger\Client\Model\SalesChannelRelationshipsMailHeaderFooter**](SalesChannelRelationshipsMailHeaderFooter.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\SalesChannelRelationshipsNewsletterRecipients**](SalesChannelRelationshipsNewsletterRecipients.md) |  | [optional] 
**mail_templates** | [**\Swagger\Client\Model\SalesChannelRelationshipsMailTemplates**](SalesChannelRelationshipsMailTemplates.md) |  | [optional] 
**number_range_sales_channels** | [**\Swagger\Client\Model\SalesChannelRelationshipsNumberRangeSalesChannels**](SalesChannelRelationshipsNumberRangeSalesChannels.md) |  | [optional] 
**promotion_sales_channels** | [**\Swagger\Client\Model\SalesChannelRelationshipsPromotionSalesChannels**](SalesChannelRelationshipsPromotionSalesChannels.md) |  | [optional] 
**document_base_config_sales_channels** | [**\Swagger\Client\Model\SalesChannelRelationshipsDocumentBaseConfigSalesChannels**](SalesChannelRelationshipsDocumentBaseConfigSalesChannels.md) |  | [optional] 
**product_reviews** | [**\Swagger\Client\Model\SalesChannelRelationshipsProductReviews**](SalesChannelRelationshipsProductReviews.md) |  | [optional] 
**seo_urls** | [**\Swagger\Client\Model\SalesChannelRelationshipsSeoUrls**](SalesChannelRelationshipsSeoUrls.md) |  | [optional] 
**seo_url_templates** | [**\Swagger\Client\Model\SalesChannelRelationshipsSeoUrlTemplates**](SalesChannelRelationshipsSeoUrlTemplates.md) |  | [optional] 
**main_categories** | [**\Swagger\Client\Model\SalesChannelRelationshipsMainCategories**](SalesChannelRelationshipsMainCategories.md) |  | [optional] 
**product_exports** | [**\Swagger\Client\Model\SalesChannelRelationshipsProductExports**](SalesChannelRelationshipsProductExports.md) |  | [optional] 
**analytics** | [**\Swagger\Client\Model\SalesChannelRelationshipsAnalytics**](SalesChannelRelationshipsAnalytics.md) |  | [optional] 
**customer_groups_registrations** | [**\Swagger\Client\Model\SalesChannelRelationshipsCustomerGroupsRegistrations**](SalesChannelRelationshipsCustomerGroupsRegistrations.md) |  | [optional] 
**event_actions** | [**\Swagger\Client\Model\SalesChannelRelationshipsEventActions**](SalesChannelRelationshipsEventActions.md) |  | [optional] 
**bound_customers** | [**\Swagger\Client\Model\SalesChannelRelationshipsBoundCustomers**](SalesChannelRelationshipsBoundCustomers.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

