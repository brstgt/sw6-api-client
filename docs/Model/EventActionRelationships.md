# EventActionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rules** | [**\Swagger\Client\Model\EventActionRelationshipsRules**](EventActionRelationshipsRules.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\EventActionRelationshipsSalesChannels**](EventActionRelationshipsSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

