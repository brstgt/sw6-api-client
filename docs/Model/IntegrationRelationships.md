# IntegrationRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | [**\Swagger\Client\Model\IntegrationRelationshipsApp**](IntegrationRelationshipsApp.md) |  | [optional] 
**acl_roles** | [**\Swagger\Client\Model\IntegrationRelationshipsAclRoles**](IntegrationRelationshipsAclRoles.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

