# AclRoleRelationshipsIntegrations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\AclRoleRelationshipsIntegrationsLinks**](AclRoleRelationshipsIntegrationsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\AclRoleRelationshipsIntegrationsData[]**](AclRoleRelationshipsIntegrationsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

