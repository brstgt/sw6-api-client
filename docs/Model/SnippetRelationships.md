# SnippetRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**set** | [**\Swagger\Client\Model\SnippetRelationshipsSet**](SnippetRelationshipsSet.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

