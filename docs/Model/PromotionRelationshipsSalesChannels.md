# PromotionRelationshipsSalesChannels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\PromotionRelationshipsSalesChannelsLinks**](PromotionRelationshipsSalesChannelsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\PromotionRelationshipsSalesChannelsData[]**](PromotionRelationshipsSalesChannelsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

