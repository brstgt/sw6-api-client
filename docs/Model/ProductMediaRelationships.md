# ProductMediaRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductMediaRelationshipsProduct**](ProductMediaRelationshipsProduct.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\ProductMediaRelationshipsMedia**](ProductMediaRelationshipsMedia.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

