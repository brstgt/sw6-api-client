# PaymentMethodRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media** | [**\Swagger\Client\Model\PaymentMethodRelationshipsMedia**](PaymentMethodRelationshipsMedia.md) |  | [optional] 
**availability_rule** | [**\Swagger\Client\Model\PaymentMethodRelationshipsAvailabilityRule**](PaymentMethodRelationshipsAvailabilityRule.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\PaymentMethodRelationshipsSalesChannelDefaultAssignments**](PaymentMethodRelationshipsSalesChannelDefaultAssignments.md) |  | [optional] 
**plugin** | [**\Swagger\Client\Model\PaymentMethodRelationshipsPlugin**](PaymentMethodRelationshipsPlugin.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\PaymentMethodRelationshipsCustomers**](PaymentMethodRelationshipsCustomers.md) |  | [optional] 
**order_transactions** | [**\Swagger\Client\Model\PaymentMethodRelationshipsOrderTransactions**](PaymentMethodRelationshipsOrderTransactions.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\PaymentMethodRelationshipsSalesChannels**](PaymentMethodRelationshipsSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

