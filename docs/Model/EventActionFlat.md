# EventActionFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**event_name** | **string** |  | 
**action_name** | **string** |  | 
**config** | **object** |  | [optional] 
**active** | **bool** |  | [optional] 
**title** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**rules** | [**\Swagger\Client\Model\RuleFlat**](RuleFlat.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

