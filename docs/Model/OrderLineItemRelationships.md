# OrderLineItemRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cover** | [**\Swagger\Client\Model\OrderLineItemRelationshipsCover**](OrderLineItemRelationshipsCover.md) |  | [optional] 
**order** | [**\Swagger\Client\Model\OrderLineItemRelationshipsOrder**](OrderLineItemRelationshipsOrder.md) |  | [optional] 
**product** | [**\Swagger\Client\Model\OrderLineItemRelationshipsProduct**](OrderLineItemRelationshipsProduct.md) |  | [optional] 
**order_delivery_positions** | [**\Swagger\Client\Model\OrderLineItemRelationshipsOrderDeliveryPositions**](OrderLineItemRelationshipsOrderDeliveryPositions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

