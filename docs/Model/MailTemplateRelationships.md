# MailTemplateRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channels** | [**\Swagger\Client\Model\MailTemplateRelationshipsSalesChannels**](MailTemplateRelationshipsSalesChannels.md) |  | [optional] 
**mail_template_type** | [**\Swagger\Client\Model\MailTemplateRelationshipsMailTemplateType**](MailTemplateRelationshipsMailTemplateType.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\MailTemplateRelationshipsMedia**](MailTemplateRelationshipsMedia.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

