# PromotionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setgroups** | [**\Swagger\Client\Model\PromotionRelationshipsSetgroups**](PromotionRelationshipsSetgroups.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\PromotionRelationshipsSalesChannels**](PromotionRelationshipsSalesChannels.md) |  | [optional] 
**discounts** | [**\Swagger\Client\Model\PromotionRelationshipsDiscounts**](PromotionRelationshipsDiscounts.md) |  | [optional] 
**individual_codes** | [**\Swagger\Client\Model\PromotionRelationshipsIndividualCodes**](PromotionRelationshipsIndividualCodes.md) |  | [optional] 
**persona_rules** | [**\Swagger\Client\Model\PromotionRelationshipsPersonaRules**](PromotionRelationshipsPersonaRules.md) |  | [optional] 
**persona_customers** | [**\Swagger\Client\Model\PromotionRelationshipsPersonaCustomers**](PromotionRelationshipsPersonaCustomers.md) |  | [optional] 
**order_rules** | [**\Swagger\Client\Model\PromotionRelationshipsOrderRules**](PromotionRelationshipsOrderRules.md) |  | [optional] 
**cart_rules** | [**\Swagger\Client\Model\PromotionRelationshipsCartRules**](PromotionRelationshipsCartRules.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

