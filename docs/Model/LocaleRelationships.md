# LocaleRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**languages** | [**\Swagger\Client\Model\LocaleRelationshipsLanguages**](LocaleRelationshipsLanguages.md) |  | [optional] 
**users** | [**\Swagger\Client\Model\LocaleRelationshipsUsers**](LocaleRelationshipsUsers.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

