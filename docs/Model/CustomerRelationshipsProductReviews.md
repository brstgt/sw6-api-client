# CustomerRelationshipsProductReviews

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CustomerRelationshipsProductReviewsLinks**](CustomerRelationshipsProductReviewsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CustomerRelationshipsProductReviewsData[]**](CustomerRelationshipsProductReviewsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

