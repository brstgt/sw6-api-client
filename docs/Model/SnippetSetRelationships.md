# SnippetSetRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snippets** | [**\Swagger\Client\Model\SnippetSetRelationshipsSnippets**](SnippetSetRelationshipsSnippets.md) |  | [optional] 
**sales_channel_domains** | [**\Swagger\Client\Model\SnippetSetRelationshipsSalesChannelDomains**](SnippetSetRelationshipsSalesChannelDomains.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

