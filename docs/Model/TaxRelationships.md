# TaxRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\Swagger\Client\Model\TaxRelationshipsProducts**](TaxRelationshipsProducts.md) |  | [optional] 
**rules** | [**\Swagger\Client\Model\TaxRelationshipsRules**](TaxRelationshipsRules.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\TaxRelationshipsShippingMethods**](TaxRelationshipsShippingMethods.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

