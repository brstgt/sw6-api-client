# MediaRelationshipsPaymentMethods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\MediaRelationshipsPaymentMethodsLinks**](MediaRelationshipsPaymentMethodsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\MediaRelationshipsPaymentMethodsData[]**](MediaRelationshipsPaymentMethodsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

