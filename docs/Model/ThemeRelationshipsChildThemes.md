# ThemeRelationshipsChildThemes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\ThemeRelationshipsChildThemesLinks**](ThemeRelationshipsChildThemesLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\ThemeRelationshipsChildThemesData[]**](ThemeRelationshipsChildThemesData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

