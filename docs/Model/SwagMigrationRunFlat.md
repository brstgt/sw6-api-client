# SwagMigrationRunFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**connection_id** | **string** |  | [optional] 
**environment_information** | **object** |  | [optional] 
**progress** | **object** |  | [optional] 
**premapping** | **object** |  | [optional] 
**user_id** | **string** |  | [optional] 
**access_token** | **string** |  | [optional] 
**status** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**connection** | [**\Swagger\Client\Model\SwagMigrationConnectionFlat**](SwagMigrationConnectionFlat.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SwagMigrationDataFlat**](SwagMigrationDataFlat.md) |  | [optional] 
**media_files** | [**\Swagger\Client\Model\SwagMigrationMediaFileFlat**](SwagMigrationMediaFileFlat.md) |  | [optional] 
**logs** | [**\Swagger\Client\Model\SwagMigrationLoggingFlat**](SwagMigrationLoggingFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

