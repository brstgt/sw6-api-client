# PickwareDhlShipmentFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipment_blueprint** | **object** |  | 
**cancelled** | **bool** |  | 
**carrier_technical_name** | **string** |  | [optional] 
**sales_channel_id** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**carrier** | [**\Swagger\Client\Model\PickwareDhlCarrierFlat**](PickwareDhlCarrierFlat.md) |  | [optional] 
**tracking_codes** | [**\Swagger\Client\Model\PickwareDhlTrackingCodeFlat**](PickwareDhlTrackingCodeFlat.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\PickwareDocumentFlat**](PickwareDocumentFlat.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\OrderDeliveryFlat**](OrderDeliveryFlat.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

