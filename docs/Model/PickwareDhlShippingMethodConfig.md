# PickwareDhlShippingMethodConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipping_method_id** | **string** |  | 
**carrier_technical_name** | **string** |  | 
**shipment_config** | **object** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**relationships** | [**\Swagger\Client\Model\PickwareDhlShippingMethodConfigRelationships**](PickwareDhlShippingMethodConfigRelationships.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

