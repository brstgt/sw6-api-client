# CustomerRecoveryRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\Swagger\Client\Model\CustomerRecoveryRelationshipsCustomer**](CustomerRecoveryRelationshipsCustomer.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

