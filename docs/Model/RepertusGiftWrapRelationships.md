# RepertusGiftWrapRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\RepertusGiftWrapRelationshipsProduct**](RepertusGiftWrapRelationshipsProduct.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\RepertusGiftWrapRelationshipsTax**](RepertusGiftWrapRelationshipsTax.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

