# OrderRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine_state** | [**\Swagger\Client\Model\OrderRelationshipsStateMachineState**](OrderRelationshipsStateMachineState.md) |  | [optional] 
**order_customer** | [**\Swagger\Client\Model\OrderRelationshipsOrderCustomer**](OrderRelationshipsOrderCustomer.md) |  | [optional] 
**currency** | [**\Swagger\Client\Model\OrderRelationshipsCurrency**](OrderRelationshipsCurrency.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\OrderRelationshipsLanguage**](OrderRelationshipsLanguage.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\OrderRelationshipsSalesChannel**](OrderRelationshipsSalesChannel.md) |  | [optional] 
**addresses** | [**\Swagger\Client\Model\OrderRelationshipsAddresses**](OrderRelationshipsAddresses.md) |  | [optional] 
**deliveries** | [**\Swagger\Client\Model\OrderRelationshipsDeliveries**](OrderRelationshipsDeliveries.md) |  | [optional] 
**line_items** | [**\Swagger\Client\Model\OrderRelationshipsLineItems**](OrderRelationshipsLineItems.md) |  | [optional] 
**transactions** | [**\Swagger\Client\Model\OrderRelationshipsTransactions**](OrderRelationshipsTransactions.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\OrderRelationshipsDocuments**](OrderRelationshipsDocuments.md) |  | [optional] 
**tags** | [**\Swagger\Client\Model\OrderRelationshipsTags**](OrderRelationshipsTags.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

