# OrderDeliveryRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine_state** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsStateMachineState**](OrderDeliveryRelationshipsStateMachineState.md) |  | [optional] 
**order** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsOrder**](OrderDeliveryRelationshipsOrder.md) |  | [optional] 
**shipping_order_address** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsShippingOrderAddress**](OrderDeliveryRelationshipsShippingOrderAddress.md) |  | [optional] 
**shipping_method** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsShippingMethod**](OrderDeliveryRelationshipsShippingMethod.md) |  | [optional] 
**positions** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsPositions**](OrderDeliveryRelationshipsPositions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

