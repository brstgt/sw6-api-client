# PickwareDocumentFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**deep_link_code** | **string** |  | 
**orientation** | **string** |  | 
**mime_type** | **string** |  | 
**page_format** | **object** |  | 
**document_type_technical_name** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**document_type** | [**\Swagger\Client\Model\PickwareDocumentTypeFlat**](PickwareDocumentTypeFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

