# AppRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**integration** | [**\Swagger\Client\Model\AppRelationshipsIntegration**](AppRelationshipsIntegration.md) |  | [optional] 
**acl_role** | [**\Swagger\Client\Model\AppRelationshipsAclRole**](AppRelationshipsAclRole.md) |  | [optional] 
**custom_field_sets** | [**\Swagger\Client\Model\AppRelationshipsCustomFieldSets**](AppRelationshipsCustomFieldSets.md) |  | [optional] 
**action_buttons** | [**\Swagger\Client\Model\AppRelationshipsActionButtons**](AppRelationshipsActionButtons.md) |  | [optional] 
**templates** | [**\Swagger\Client\Model\AppRelationshipsTemplates**](AppRelationshipsTemplates.md) |  | [optional] 
**webhooks** | [**\Swagger\Client\Model\AppRelationshipsWebhooks**](AppRelationshipsWebhooks.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

