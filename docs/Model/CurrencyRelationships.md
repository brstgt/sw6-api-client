# CurrencyRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channel_default_assignments** | [**\Swagger\Client\Model\CurrencyRelationshipsSalesChannelDefaultAssignments**](CurrencyRelationshipsSalesChannelDefaultAssignments.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\CurrencyRelationshipsOrders**](CurrencyRelationshipsOrders.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\CurrencyRelationshipsSalesChannels**](CurrencyRelationshipsSalesChannels.md) |  | [optional] 
**sales_channel_domains** | [**\Swagger\Client\Model\CurrencyRelationshipsSalesChannelDomains**](CurrencyRelationshipsSalesChannelDomains.md) |  | [optional] 
**promotion_discount_prices** | [**\Swagger\Client\Model\CurrencyRelationshipsPromotionDiscountPrices**](CurrencyRelationshipsPromotionDiscountPrices.md) |  | [optional] 
**product_exports** | [**\Swagger\Client\Model\CurrencyRelationshipsProductExports**](CurrencyRelationshipsProductExports.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

