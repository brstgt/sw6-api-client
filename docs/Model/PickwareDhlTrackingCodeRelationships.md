# PickwareDhlTrackingCodeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment** | [**\Swagger\Client\Model\PickwareDhlTrackingCodeRelationshipsShipment**](PickwareDhlTrackingCodeRelationshipsShipment.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\PickwareDhlTrackingCodeRelationshipsDocuments**](PickwareDhlTrackingCodeRelationshipsDocuments.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

