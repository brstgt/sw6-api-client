# IntegrationRoleFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**integration_id** | **string** |  | 
**acl_role_id** | **string** |  | 
**integration** | [**\Swagger\Client\Model\IntegrationFlat**](IntegrationFlat.md) |  | [optional] 
**role** | [**\Swagger\Client\Model\AclRoleFlat**](AclRoleFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

