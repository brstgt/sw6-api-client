# NumberRangeSalesChannelRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number_range** | [**\Swagger\Client\Model\NumberRangeSalesChannelRelationshipsNumberRange**](NumberRangeSalesChannelRelationshipsNumberRange.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\NumberRangeSalesChannelRelationshipsSalesChannel**](NumberRangeSalesChannelRelationshipsSalesChannel.md) |  | [optional] 
**number_range_type** | [**\Swagger\Client\Model\NumberRangeSalesChannelRelationshipsNumberRangeType**](NumberRangeSalesChannelRelationshipsNumberRangeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

