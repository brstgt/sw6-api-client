# SwagMigrationGeneralSettingRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**selected_connection** | [**\Swagger\Client\Model\SwagMigrationGeneralSettingRelationshipsSelectedConnection**](SwagMigrationGeneralSettingRelationshipsSelectedConnection.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

