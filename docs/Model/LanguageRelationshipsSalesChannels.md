# LanguageRelationshipsSalesChannels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\LanguageRelationshipsSalesChannelsLinks**](LanguageRelationshipsSalesChannelsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\LanguageRelationshipsSalesChannelsData[]**](LanguageRelationshipsSalesChannelsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

