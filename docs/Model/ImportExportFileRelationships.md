# ImportExportFileRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**log** | [**\Swagger\Client\Model\ImportExportFileRelationshipsLog**](ImportExportFileRelationshipsLog.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

