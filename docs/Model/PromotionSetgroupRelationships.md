# PromotionSetgroupRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion** | [**\Swagger\Client\Model\PromotionSetgroupRelationshipsPromotion**](PromotionSetgroupRelationshipsPromotion.md) |  | [optional] 
**set_group_rules** | [**\Swagger\Client\Model\PromotionSetgroupRelationshipsSetGroupRules**](PromotionSetgroupRelationshipsSetGroupRules.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

