# PromotionDiscountPricesRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion_discount** | [**\Swagger\Client\Model\PromotionDiscountPricesRelationshipsPromotionDiscount**](PromotionDiscountPricesRelationshipsPromotionDiscount.md) |  | [optional] 
**currency** | [**\Swagger\Client\Model\PromotionDiscountPricesRelationshipsCurrency**](PromotionDiscountPricesRelationshipsCurrency.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

