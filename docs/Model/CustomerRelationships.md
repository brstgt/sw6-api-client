# CustomerRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | [**\Swagger\Client\Model\CustomerRelationshipsGroup**](CustomerRelationshipsGroup.md) |  | [optional] 
**default_payment_method** | [**\Swagger\Client\Model\CustomerRelationshipsDefaultPaymentMethod**](CustomerRelationshipsDefaultPaymentMethod.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\CustomerRelationshipsSalesChannel**](CustomerRelationshipsSalesChannel.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\CustomerRelationshipsLanguage**](CustomerRelationshipsLanguage.md) |  | [optional] 
**last_payment_method** | [**\Swagger\Client\Model\CustomerRelationshipsLastPaymentMethod**](CustomerRelationshipsLastPaymentMethod.md) |  | [optional] 
**default_billing_address** | [**\Swagger\Client\Model\CustomerRelationshipsDefaultBillingAddress**](CustomerRelationshipsDefaultBillingAddress.md) |  | [optional] 
**default_shipping_address** | [**\Swagger\Client\Model\CustomerRelationshipsDefaultShippingAddress**](CustomerRelationshipsDefaultShippingAddress.md) |  | [optional] 
**salutation** | [**\Swagger\Client\Model\CustomerRelationshipsSalutation**](CustomerRelationshipsSalutation.md) |  | [optional] 
**addresses** | [**\Swagger\Client\Model\CustomerRelationshipsAddresses**](CustomerRelationshipsAddresses.md) |  | [optional] 
**order_customers** | [**\Swagger\Client\Model\CustomerRelationshipsOrderCustomers**](CustomerRelationshipsOrderCustomers.md) |  | [optional] 
**tags** | [**\Swagger\Client\Model\CustomerRelationshipsTags**](CustomerRelationshipsTags.md) |  | [optional] 
**promotions** | [**\Swagger\Client\Model\CustomerRelationshipsPromotions**](CustomerRelationshipsPromotions.md) |  | [optional] 
**product_reviews** | [**\Swagger\Client\Model\CustomerRelationshipsProductReviews**](CustomerRelationshipsProductReviews.md) |  | [optional] 
**recovery_customer** | [**\Swagger\Client\Model\CustomerRelationshipsRecoveryCustomer**](CustomerRelationshipsRecoveryCustomer.md) |  | [optional] 
**requested_group** | [**\Swagger\Client\Model\CustomerRelationshipsRequestedGroup**](CustomerRelationshipsRequestedGroup.md) |  | [optional] 
**bound_sales_channel** | [**\Swagger\Client\Model\CustomerRelationshipsBoundSalesChannel**](CustomerRelationshipsBoundSalesChannel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

