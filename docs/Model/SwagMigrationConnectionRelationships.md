# SwagMigrationConnectionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**runs** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsRuns**](SwagMigrationConnectionRelationshipsRuns.md) |  | [optional] 
**mappings** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsMappings**](SwagMigrationConnectionRelationshipsMappings.md) |  | [optional] 
**settings** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsSettings**](SwagMigrationConnectionRelationshipsSettings.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

