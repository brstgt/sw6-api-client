# InlineResponse20092

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Swagger\Client\Model\KlarnaPaymentButtonKey**](KlarnaPaymentButtonKey.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

