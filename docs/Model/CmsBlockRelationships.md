# CmsBlockRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**section** | [**\Swagger\Client\Model\CmsBlockRelationshipsSection**](CmsBlockRelationshipsSection.md) |  | [optional] 
**background_media** | [**\Swagger\Client\Model\CmsBlockRelationshipsBackgroundMedia**](CmsBlockRelationshipsBackgroundMedia.md) |  | [optional] 
**slots** | [**\Swagger\Client\Model\CmsBlockRelationshipsSlots**](CmsBlockRelationshipsSlots.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

