# ProductConfiguratorSettingRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductConfiguratorSettingRelationshipsProduct**](ProductConfiguratorSettingRelationshipsProduct.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\ProductConfiguratorSettingRelationshipsMedia**](ProductConfiguratorSettingRelationshipsMedia.md) |  | [optional] 
**option** | [**\Swagger\Client\Model\ProductConfiguratorSettingRelationshipsOption**](ProductConfiguratorSettingRelationshipsOption.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

