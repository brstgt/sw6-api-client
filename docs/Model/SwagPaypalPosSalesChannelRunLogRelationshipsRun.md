# SwagPaypalPosSalesChannelRunLogRelationshipsRun

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRunLogRelationshipsRunLinks**](SwagPaypalPosSalesChannelRunLogRelationshipsRunLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRunLogRelationshipsRunData**](SwagPaypalPosSalesChannelRunLogRelationshipsRunData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

