# MediaRelationshipsTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\MediaRelationshipsTagsLinks**](MediaRelationshipsTagsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\MediaRelationshipsTagsData[]**](MediaRelationshipsTagsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

