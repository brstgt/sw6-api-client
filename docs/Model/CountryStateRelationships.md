# CountryStateRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | [**\Swagger\Client\Model\CountryStateRelationshipsCountry**](CountryStateRelationshipsCountry.md) |  | [optional] 
**customer_addresses** | [**\Swagger\Client\Model\CountryStateRelationshipsCustomerAddresses**](CountryStateRelationshipsCustomerAddresses.md) |  | [optional] 
**order_addresses** | [**\Swagger\Client\Model\CountryStateRelationshipsOrderAddresses**](CountryStateRelationshipsOrderAddresses.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

