# OrderDeliveryRelationshipsShippingOrderAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsShippingOrderAddressLinks**](OrderDeliveryRelationshipsShippingOrderAddressLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\OrderDeliveryRelationshipsShippingOrderAddressData**](OrderDeliveryRelationshipsShippingOrderAddressData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

