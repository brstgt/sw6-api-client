# OrderAddressRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | [**\Swagger\Client\Model\OrderAddressRelationshipsCountry**](OrderAddressRelationshipsCountry.md) |  | [optional] 
**country_state** | [**\Swagger\Client\Model\OrderAddressRelationshipsCountryState**](OrderAddressRelationshipsCountryState.md) |  | [optional] 
**order** | [**\Swagger\Client\Model\OrderAddressRelationshipsOrder**](OrderAddressRelationshipsOrder.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\OrderAddressRelationshipsOrderDeliveries**](OrderAddressRelationshipsOrderDeliveries.md) |  | [optional] 
**salutation** | [**\Swagger\Client\Model\OrderAddressRelationshipsSalutation**](OrderAddressRelationshipsSalutation.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

