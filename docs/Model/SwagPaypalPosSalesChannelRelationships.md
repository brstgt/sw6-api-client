# SwagPaypalPosSalesChannelRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channel** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRelationshipsSalesChannel**](SwagPaypalPosSalesChannelRelationshipsSalesChannel.md) |  | [optional] 
**product_stream** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRelationshipsProductStream**](SwagPaypalPosSalesChannelRelationshipsProductStream.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

