# StateMachineTransitionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine** | [**\Swagger\Client\Model\StateMachineTransitionRelationshipsStateMachine**](StateMachineTransitionRelationshipsStateMachine.md) |  | [optional] 
**from_state_machine_state** | [**\Swagger\Client\Model\StateMachineTransitionRelationshipsFromStateMachineState**](StateMachineTransitionRelationshipsFromStateMachineState.md) |  | [optional] 
**to_state_machine_state** | [**\Swagger\Client\Model\StateMachineTransitionRelationshipsToStateMachineState**](StateMachineTransitionRelationshipsToStateMachineState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

