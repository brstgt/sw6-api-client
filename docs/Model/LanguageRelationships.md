# LanguageRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**\Swagger\Client\Model\LanguageRelationshipsParent**](LanguageRelationshipsParent.md) |  | [optional] 
**locale** | [**\Swagger\Client\Model\LanguageRelationshipsLocale**](LanguageRelationshipsLocale.md) |  | [optional] 
**translation_code** | [**\Swagger\Client\Model\LanguageRelationshipsTranslationCode**](LanguageRelationshipsTranslationCode.md) |  | [optional] 
**children** | [**\Swagger\Client\Model\LanguageRelationshipsChildren**](LanguageRelationshipsChildren.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\LanguageRelationshipsSalesChannels**](LanguageRelationshipsSalesChannels.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\LanguageRelationshipsSalesChannelDefaultAssignments**](LanguageRelationshipsSalesChannelDefaultAssignments.md) |  | [optional] 
**sales_channel_domains** | [**\Swagger\Client\Model\LanguageRelationshipsSalesChannelDomains**](LanguageRelationshipsSalesChannelDomains.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\LanguageRelationshipsCustomers**](LanguageRelationshipsCustomers.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\LanguageRelationshipsNewsletterRecipients**](LanguageRelationshipsNewsletterRecipients.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\LanguageRelationshipsOrders**](LanguageRelationshipsOrders.md) |  | [optional] 
**product_search_keywords** | [**\Swagger\Client\Model\LanguageRelationshipsProductSearchKeywords**](LanguageRelationshipsProductSearchKeywords.md) |  | [optional] 
**product_keyword_dictionaries** | [**\Swagger\Client\Model\LanguageRelationshipsProductKeywordDictionaries**](LanguageRelationshipsProductKeywordDictionaries.md) |  | [optional] 
**product_reviews** | [**\Swagger\Client\Model\LanguageRelationshipsProductReviews**](LanguageRelationshipsProductReviews.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

