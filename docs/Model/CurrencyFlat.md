# CurrencyFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**factor** | **float** |  | 
**symbol** | **string** |  | 
**iso_code** | **string** |  | 
**short_name** | **string** |  | 
**name** | **string** |  | 
**decimal_precision** | **int** |  | 
**position** | **int** |  | [optional] 
**is_system_default** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\OrderFlat**](OrderFlat.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**sales_channel_domains** | [**\Swagger\Client\Model\SalesChannelDomainFlat**](SalesChannelDomainFlat.md) |  | [optional] 
**promotion_discount_prices** | [**\Swagger\Client\Model\PromotionDiscountPricesFlat**](PromotionDiscountPricesFlat.md) |  | [optional] 
**product_exports** | [**\Swagger\Client\Model\ProductExportFlat**](ProductExportFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

