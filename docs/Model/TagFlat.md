# TagFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\ProductFlat**](ProductFlat.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\MediaFlat**](MediaFlat.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\CategoryFlat**](CategoryFlat.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\CustomerFlat**](CustomerFlat.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\OrderFlat**](OrderFlat.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\ShippingMethodFlat**](ShippingMethodFlat.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\NewsletterRecipientFlat**](NewsletterRecipientFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

