# PropertyGroupOptionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media** | [**\Swagger\Client\Model\PropertyGroupOptionRelationshipsMedia**](PropertyGroupOptionRelationshipsMedia.md) |  | [optional] 
**group** | [**\Swagger\Client\Model\PropertyGroupOptionRelationshipsGroup**](PropertyGroupOptionRelationshipsGroup.md) |  | [optional] 
**product_configurator_settings** | [**\Swagger\Client\Model\PropertyGroupOptionRelationshipsProductConfiguratorSettings**](PropertyGroupOptionRelationshipsProductConfiguratorSettings.md) |  | [optional] 
**product_properties** | [**\Swagger\Client\Model\PropertyGroupOptionRelationshipsProductProperties**](PropertyGroupOptionRelationshipsProductProperties.md) |  | [optional] 
**product_options** | [**\Swagger\Client\Model\PropertyGroupOptionRelationshipsProductOptions**](PropertyGroupOptionRelationshipsProductOptions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

