# ProductReviewRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductReviewRelationshipsProduct**](ProductReviewRelationshipsProduct.md) |  | [optional] 
**customer** | [**\Swagger\Client\Model\ProductReviewRelationshipsCustomer**](ProductReviewRelationshipsCustomer.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\ProductReviewRelationshipsSalesChannel**](ProductReviewRelationshipsSalesChannel.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\ProductReviewRelationshipsLanguage**](ProductReviewRelationshipsLanguage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

