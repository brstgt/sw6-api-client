# PromotionDiscountRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion** | [**\Swagger\Client\Model\PromotionDiscountRelationshipsPromotion**](PromotionDiscountRelationshipsPromotion.md) |  | [optional] 
**discount_rules** | [**\Swagger\Client\Model\PromotionDiscountRelationshipsDiscountRules**](PromotionDiscountRelationshipsDiscountRules.md) |  | [optional] 
**promotion_discount_prices** | [**\Swagger\Client\Model\PromotionDiscountRelationshipsPromotionDiscountPrices**](PromotionDiscountRelationshipsPromotionDiscountPrices.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

