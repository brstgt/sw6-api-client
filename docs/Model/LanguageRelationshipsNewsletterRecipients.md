# LanguageRelationshipsNewsletterRecipients

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\LanguageRelationshipsNewsletterRecipientsLinks**](LanguageRelationshipsNewsletterRecipientsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\LanguageRelationshipsNewsletterRecipientsData[]**](LanguageRelationshipsNewsletterRecipientsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

