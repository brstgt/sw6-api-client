# StateMachineHistoryRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine** | [**\Swagger\Client\Model\StateMachineHistoryRelationshipsStateMachine**](StateMachineHistoryRelationshipsStateMachine.md) |  | [optional] 
**from_state_machine_state** | [**\Swagger\Client\Model\StateMachineHistoryRelationshipsFromStateMachineState**](StateMachineHistoryRelationshipsFromStateMachineState.md) |  | [optional] 
**to_state_machine_state** | [**\Swagger\Client\Model\StateMachineHistoryRelationshipsToStateMachineState**](StateMachineHistoryRelationshipsToStateMachineState.md) |  | [optional] 
**user** | [**\Swagger\Client\Model\StateMachineHistoryRelationshipsUser**](StateMachineHistoryRelationshipsUser.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

