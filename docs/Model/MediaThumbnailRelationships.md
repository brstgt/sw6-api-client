# MediaThumbnailRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media** | [**\Swagger\Client\Model\MediaThumbnailRelationshipsMedia**](MediaThumbnailRelationshipsMedia.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

