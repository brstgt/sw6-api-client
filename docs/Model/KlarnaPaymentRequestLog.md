# KlarnaPaymentRequestLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**klarna_order_id** | **string** |  | 
**call_type** | **string** |  | 
**request** | **object** |  | 
**response** | **object** |  | 
**idempotency_key** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

