# ShippingMethodRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_time** | [**\Swagger\Client\Model\ShippingMethodRelationshipsDeliveryTime**](ShippingMethodRelationshipsDeliveryTime.md) |  | [optional] 
**availability_rule** | [**\Swagger\Client\Model\ShippingMethodRelationshipsAvailabilityRule**](ShippingMethodRelationshipsAvailabilityRule.md) |  | [optional] 
**prices** | [**\Swagger\Client\Model\ShippingMethodRelationshipsPrices**](ShippingMethodRelationshipsPrices.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\ShippingMethodRelationshipsMedia**](ShippingMethodRelationshipsMedia.md) |  | [optional] 
**tags** | [**\Swagger\Client\Model\ShippingMethodRelationshipsTags**](ShippingMethodRelationshipsTags.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\ShippingMethodRelationshipsOrderDeliveries**](ShippingMethodRelationshipsOrderDeliveries.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\ShippingMethodRelationshipsSalesChannels**](ShippingMethodRelationshipsSalesChannels.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\ShippingMethodRelationshipsSalesChannelDefaultAssignments**](ShippingMethodRelationshipsSalesChannelDefaultAssignments.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\ShippingMethodRelationshipsTax**](ShippingMethodRelationshipsTax.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

