# SalesChannelRelationshipsOrders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsOrdersLinks**](SalesChannelRelationshipsOrdersLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SalesChannelRelationshipsOrdersData[]**](SalesChannelRelationshipsOrdersData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

