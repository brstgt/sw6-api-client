# CmsSlotRelationshipsBlock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CmsSlotRelationshipsBlockLinks**](CmsSlotRelationshipsBlockLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CmsSlotRelationshipsBlockData**](CmsSlotRelationshipsBlockData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

