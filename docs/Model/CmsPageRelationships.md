# CmsPageRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sections** | [**\Swagger\Client\Model\CmsPageRelationshipsSections**](CmsPageRelationshipsSections.md) |  | [optional] 
**preview_media** | [**\Swagger\Client\Model\CmsPageRelationshipsPreviewMedia**](CmsPageRelationshipsPreviewMedia.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\CmsPageRelationshipsCategories**](CmsPageRelationshipsCategories.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

