# SwagPaypalPosSalesChannelRunFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**sales_channel_id** | **string** |  | 
**task** | **string** |  | 
**finished_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**aborted_by_user** | **bool** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**logs** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRunLogFlat**](SwagPaypalPosSalesChannelRunLogFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

