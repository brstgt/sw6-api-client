# RepertusGiftWrapFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**master** | **bool** |  | 
**product_id** | **string** |  | 
**product_version_id** | **string** |  | [optional] 
**tax_id** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**product** | [**\Swagger\Client\Model\ProductFlat**](ProductFlat.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\TaxFlat**](TaxFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

