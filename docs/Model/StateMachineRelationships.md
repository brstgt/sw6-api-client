# StateMachineRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**states** | [**\Swagger\Client\Model\StateMachineRelationshipsStates**](StateMachineRelationshipsStates.md) |  | [optional] 
**transitions** | [**\Swagger\Client\Model\StateMachineRelationshipsTransitions**](StateMachineRelationshipsTransitions.md) |  | [optional] 
**history_entries** | [**\Swagger\Client\Model\StateMachineRelationshipsHistoryEntries**](StateMachineRelationshipsHistoryEntries.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

