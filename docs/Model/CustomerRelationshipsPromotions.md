# CustomerRelationshipsPromotions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CustomerRelationshipsPromotionsLinks**](CustomerRelationshipsPromotionsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CustomerRelationshipsPromotionsData[]**](CustomerRelationshipsPromotionsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

