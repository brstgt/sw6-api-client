# SwagMigrationRunRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection** | [**\Swagger\Client\Model\SwagMigrationRunRelationshipsConnection**](SwagMigrationRunRelationshipsConnection.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SwagMigrationRunRelationshipsData**](SwagMigrationRunRelationshipsData.md) |  | [optional] 
**media_files** | [**\Swagger\Client\Model\SwagMigrationRunRelationshipsMediaFiles**](SwagMigrationRunRelationshipsMediaFiles.md) |  | [optional] 
**logs** | [**\Swagger\Client\Model\SwagMigrationRunRelationshipsLogs**](SwagMigrationRunRelationshipsLogs.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

