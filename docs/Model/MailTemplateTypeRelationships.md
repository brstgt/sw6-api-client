# MailTemplateTypeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mail_templates** | [**\Swagger\Client\Model\MailTemplateTypeRelationshipsMailTemplates**](MailTemplateTypeRelationshipsMailTemplates.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\MailTemplateTypeRelationshipsSalesChannels**](MailTemplateTypeRelationshipsSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

