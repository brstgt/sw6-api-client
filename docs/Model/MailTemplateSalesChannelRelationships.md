# MailTemplateSalesChannelRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mail_template_type** | [**\Swagger\Client\Model\MailTemplateSalesChannelRelationshipsMailTemplateType**](MailTemplateSalesChannelRelationshipsMailTemplateType.md) |  | [optional] 
**mail_template** | [**\Swagger\Client\Model\MailTemplateSalesChannelRelationshipsMailTemplate**](MailTemplateSalesChannelRelationshipsMailTemplate.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\MailTemplateSalesChannelRelationshipsSalesChannel**](MailTemplateSalesChannelRelationshipsSalesChannel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

