# ThemeRelationshipsSalesChannels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\ThemeRelationshipsSalesChannelsLinks**](ThemeRelationshipsSalesChannelsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\ThemeRelationshipsSalesChannelsData[]**](ThemeRelationshipsSalesChannelsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

