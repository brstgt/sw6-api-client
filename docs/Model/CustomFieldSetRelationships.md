# CustomFieldSetRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_fields** | [**\Swagger\Client\Model\CustomFieldSetRelationshipsCustomFields**](CustomFieldSetRelationshipsCustomFields.md) |  | [optional] 
**relations** | [**\Swagger\Client\Model\CustomFieldSetRelationshipsRelations**](CustomFieldSetRelationshipsRelations.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\CustomFieldSetRelationshipsProducts**](CustomFieldSetRelationshipsProducts.md) |  | [optional] 
**app** | [**\Swagger\Client\Model\CustomFieldSetRelationshipsApp**](CustomFieldSetRelationshipsApp.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

