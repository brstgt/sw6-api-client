# CustomerTagFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**customer_id** | **string** |  | 
**tag_id** | **string** |  | 
**customer** | [**\Swagger\Client\Model\CustomerFlat**](CustomerFlat.md) |  | [optional] 
**tag** | [**\Swagger\Client\Model\TagFlat**](TagFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

