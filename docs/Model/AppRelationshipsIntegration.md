# AppRelationshipsIntegration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\AppRelationshipsIntegrationLinks**](AppRelationshipsIntegrationLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\AppRelationshipsIntegrationData**](AppRelationshipsIntegrationData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

