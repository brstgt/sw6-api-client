# AclRoleRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | [**\Swagger\Client\Model\AclRoleRelationshipsUsers**](AclRoleRelationshipsUsers.md) |  | [optional] 
**app** | [**\Swagger\Client\Model\AclRoleRelationshipsApp**](AclRoleRelationshipsApp.md) |  | [optional] 
**integrations** | [**\Swagger\Client\Model\AclRoleRelationshipsIntegrations**](AclRoleRelationshipsIntegrations.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

