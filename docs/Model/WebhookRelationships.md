# WebhookRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | [**\Swagger\Client\Model\WebhookRelationshipsApp**](WebhookRelationshipsApp.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

