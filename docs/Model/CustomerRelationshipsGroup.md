# CustomerRelationshipsGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CustomerRelationshipsGroupLinks**](CustomerRelationshipsGroupLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CustomerRelationshipsGroupData**](CustomerRelationshipsGroupData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

