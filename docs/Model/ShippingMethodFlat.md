# ShippingMethodFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**active** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**availability_rule_id** | **string** |  | 
**media_id** | **string** |  | [optional] 
**delivery_time_id** | **string** |  | 
**tax_type** | **string** |  | 
**tax_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**tracking_url** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**delivery_time** | [**\Swagger\Client\Model\DeliveryTimeFlat**](DeliveryTimeFlat.md) |  | [optional] 
**availability_rule** | [**\Swagger\Client\Model\RuleFlat**](RuleFlat.md) |  | [optional] 
**prices** | [**\Swagger\Client\Model\ShippingMethodPriceFlat**](ShippingMethodPriceFlat.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\MediaFlat**](MediaFlat.md) |  | [optional] 
**tags** | [**\Swagger\Client\Model\TagFlat**](TagFlat.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\OrderDeliveryFlat**](OrderDeliveryFlat.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\TaxFlat**](TaxFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

