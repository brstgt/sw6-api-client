# DocumentBaseConfigRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | [**\Swagger\Client\Model\DocumentBaseConfigRelationshipsDocumentType**](DocumentBaseConfigRelationshipsDocumentType.md) |  | [optional] 
**logo** | [**\Swagger\Client\Model\DocumentBaseConfigRelationshipsLogo**](DocumentBaseConfigRelationshipsLogo.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\DocumentBaseConfigRelationshipsSalesChannels**](DocumentBaseConfigRelationshipsSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

