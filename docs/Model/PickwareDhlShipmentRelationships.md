# PickwareDhlShipmentRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsCarrier**](PickwareDhlShipmentRelationshipsCarrier.md) |  | [optional] 
**tracking_codes** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsTrackingCodes**](PickwareDhlShipmentRelationshipsTrackingCodes.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsDocuments**](PickwareDhlShipmentRelationshipsDocuments.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsOrderDeliveries**](PickwareDhlShipmentRelationshipsOrderDeliveries.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsSalesChannel**](PickwareDhlShipmentRelationshipsSalesChannel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

