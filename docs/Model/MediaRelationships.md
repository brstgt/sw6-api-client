# MediaRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tags** | [**\Swagger\Client\Model\MediaRelationshipsTags**](MediaRelationshipsTags.md) |  | [optional] 
**thumbnails** | [**\Swagger\Client\Model\MediaRelationshipsThumbnails**](MediaRelationshipsThumbnails.md) |  | [optional] 
**user** | [**\Swagger\Client\Model\MediaRelationshipsUser**](MediaRelationshipsUser.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\MediaRelationshipsCategories**](MediaRelationshipsCategories.md) |  | [optional] 
**product_manufacturers** | [**\Swagger\Client\Model\MediaRelationshipsProductManufacturers**](MediaRelationshipsProductManufacturers.md) |  | [optional] 
**product_media** | [**\Swagger\Client\Model\MediaRelationshipsProductMedia**](MediaRelationshipsProductMedia.md) |  | [optional] 
**avatar_user** | [**\Swagger\Client\Model\MediaRelationshipsAvatarUser**](MediaRelationshipsAvatarUser.md) |  | [optional] 
**media_folder** | [**\Swagger\Client\Model\MediaRelationshipsMediaFolder**](MediaRelationshipsMediaFolder.md) |  | [optional] 
**property_group_options** | [**\Swagger\Client\Model\MediaRelationshipsPropertyGroupOptions**](MediaRelationshipsPropertyGroupOptions.md) |  | [optional] 
**mail_template_media** | [**\Swagger\Client\Model\MediaRelationshipsMailTemplateMedia**](MediaRelationshipsMailTemplateMedia.md) |  | [optional] 
**document_base_configs** | [**\Swagger\Client\Model\MediaRelationshipsDocumentBaseConfigs**](MediaRelationshipsDocumentBaseConfigs.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\MediaRelationshipsShippingMethods**](MediaRelationshipsShippingMethods.md) |  | [optional] 
**payment_methods** | [**\Swagger\Client\Model\MediaRelationshipsPaymentMethods**](MediaRelationshipsPaymentMethods.md) |  | [optional] 
**product_configurator_settings** | [**\Swagger\Client\Model\MediaRelationshipsProductConfiguratorSettings**](MediaRelationshipsProductConfiguratorSettings.md) |  | [optional] 
**order_line_items** | [**\Swagger\Client\Model\MediaRelationshipsOrderLineItems**](MediaRelationshipsOrderLineItems.md) |  | [optional] 
**cms_blocks** | [**\Swagger\Client\Model\MediaRelationshipsCmsBlocks**](MediaRelationshipsCmsBlocks.md) |  | [optional] 
**cms_sections** | [**\Swagger\Client\Model\MediaRelationshipsCmsSections**](MediaRelationshipsCmsSections.md) |  | [optional] 
**cms_pages** | [**\Swagger\Client\Model\MediaRelationshipsCmsPages**](MediaRelationshipsCmsPages.md) |  | [optional] 
**documents** | [**\Swagger\Client\Model\MediaRelationshipsDocuments**](MediaRelationshipsDocuments.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

