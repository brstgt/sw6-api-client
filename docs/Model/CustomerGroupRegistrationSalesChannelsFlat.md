# CustomerGroupRegistrationSalesChannelsFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**customer_group_id** | **string** |  | 
**sales_channel_id** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**customer_group** | [**\Swagger\Client\Model\CustomerGroupFlat**](CustomerGroupFlat.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

