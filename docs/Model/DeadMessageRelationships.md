# DeadMessageRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scheduled_task** | [**\Swagger\Client\Model\DeadMessageRelationshipsScheduledTask**](DeadMessageRelationshipsScheduledTask.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

