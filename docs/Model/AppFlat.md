# AppFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**path** | **string** |  | 
**author** | **string** |  | [optional] 
**copyright** | **string** |  | [optional] 
**license** | **string** |  | [optional] 
**active** | **bool** |  | 
**configurable** | **bool** |  | 
**privacy** | **string** |  | [optional] 
**version** | **string** |  | 
**icon** | **string** |  | [optional] 
**modules** | **object[]** |  | [optional] 
**cookies** | **object[]** |  | [optional] 
**label** | **string** |  | 
**description** | **string** |  | [optional] 
**privacy_policy_extensions** | **string** |  | [optional] 
**integration_id** | **string** |  | 
**acl_role_id** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**integration** | [**\Swagger\Client\Model\IntegrationFlat**](IntegrationFlat.md) |  | [optional] 
**acl_role** | [**\Swagger\Client\Model\AclRoleFlat**](AclRoleFlat.md) |  | [optional] 
**custom_field_sets** | [**\Swagger\Client\Model\CustomFieldSetFlat**](CustomFieldSetFlat.md) |  | [optional] 
**action_buttons** | [**\Swagger\Client\Model\AppActionButtonFlat**](AppActionButtonFlat.md) |  | [optional] 
**templates** | [**\Swagger\Client\Model\AppTemplateFlat**](AppTemplateFlat.md) |  | [optional] 
**webhooks** | [**\Swagger\Client\Model\WebhookFlat**](WebhookFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

