# AclRoleFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**privileges** | **object[]** |  | 
**users** | [**\Swagger\Client\Model\UserFlat**](UserFlat.md) |  | [optional] 
**app** | [**\Swagger\Client\Model\AppFlat**](AppFlat.md) |  | [optional] 
**integrations** | [**\Swagger\Client\Model\IntegrationFlat**](IntegrationFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

