# PickwareDhlShipmentOrderDeliveryMappingFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipment_id** | **string** |  | 
**order_delivery_id** | **string** |  | 
**order_delivery_version_id** | **string** |  | [optional] 
**shipment** | [**\Swagger\Client\Model\PickwareDhlShipmentFlat**](PickwareDhlShipmentFlat.md) |  | [optional] 
**order_delivery** | [**\Swagger\Client\Model\OrderDeliveryFlat**](OrderDeliveryFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

