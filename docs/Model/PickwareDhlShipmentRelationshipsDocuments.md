# PickwareDhlShipmentRelationshipsDocuments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsDocumentsLinks**](PickwareDhlShipmentRelationshipsDocumentsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\PickwareDhlShipmentRelationshipsDocumentsData[]**](PickwareDhlShipmentRelationshipsDocumentsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

