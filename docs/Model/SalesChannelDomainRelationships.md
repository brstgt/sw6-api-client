# SalesChannelDomainRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channel** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsSalesChannel**](SalesChannelDomainRelationshipsSalesChannel.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsLanguage**](SalesChannelDomainRelationshipsLanguage.md) |  | [optional] 
**currency** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsCurrency**](SalesChannelDomainRelationshipsCurrency.md) |  | [optional] 
**snippet_set** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsSnippetSet**](SalesChannelDomainRelationshipsSnippetSet.md) |  | [optional] 
**sales_channel_default_hreflang** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsSalesChannelDefaultHreflang**](SalesChannelDomainRelationshipsSalesChannelDefaultHreflang.md) |  | [optional] 
**product_exports** | [**\Swagger\Client\Model\SalesChannelDomainRelationshipsProductExports**](SalesChannelDomainRelationshipsProductExports.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

