# ShippingMethodPriceRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_method** | [**\Swagger\Client\Model\ShippingMethodPriceRelationshipsShippingMethod**](ShippingMethodPriceRelationshipsShippingMethod.md) |  | [optional] 
**rule** | [**\Swagger\Client\Model\ShippingMethodPriceRelationshipsRule**](ShippingMethodPriceRelationshipsRule.md) |  | [optional] 
**calculation_rule** | [**\Swagger\Client\Model\ShippingMethodPriceRelationshipsCalculationRule**](ShippingMethodPriceRelationshipsCalculationRule.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

