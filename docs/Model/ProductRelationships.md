# ProductRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**\Swagger\Client\Model\ProductRelationshipsParent**](ProductRelationshipsParent.md) |  | [optional] 
**children** | [**\Swagger\Client\Model\ProductRelationshipsChildren**](ProductRelationshipsChildren.md) |  | [optional] 
**delivery_time** | [**\Swagger\Client\Model\ProductRelationshipsDeliveryTime**](ProductRelationshipsDeliveryTime.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\ProductRelationshipsTax**](ProductRelationshipsTax.md) |  | [optional] 
**manufacturer** | [**\Swagger\Client\Model\ProductRelationshipsManufacturer**](ProductRelationshipsManufacturer.md) |  | [optional] 
**unit** | [**\Swagger\Client\Model\ProductRelationshipsUnit**](ProductRelationshipsUnit.md) |  | [optional] 
**cover** | [**\Swagger\Client\Model\ProductRelationshipsCover**](ProductRelationshipsCover.md) |  | [optional] 
**prices** | [**\Swagger\Client\Model\ProductRelationshipsPrices**](ProductRelationshipsPrices.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\ProductRelationshipsMedia**](ProductRelationshipsMedia.md) |  | [optional] 
**cross_sellings** | [**\Swagger\Client\Model\ProductRelationshipsCrossSellings**](ProductRelationshipsCrossSellings.md) |  | [optional] 
**cross_selling_assigned_products** | [**\Swagger\Client\Model\ProductRelationshipsCrossSellingAssignedProducts**](ProductRelationshipsCrossSellingAssignedProducts.md) |  | [optional] 
**configurator_settings** | [**\Swagger\Client\Model\ProductRelationshipsConfiguratorSettings**](ProductRelationshipsConfiguratorSettings.md) |  | [optional] 
**visibilities** | [**\Swagger\Client\Model\ProductRelationshipsVisibilities**](ProductRelationshipsVisibilities.md) |  | [optional] 
**search_keywords** | [**\Swagger\Client\Model\ProductRelationshipsSearchKeywords**](ProductRelationshipsSearchKeywords.md) |  | [optional] 
**product_reviews** | [**\Swagger\Client\Model\ProductRelationshipsProductReviews**](ProductRelationshipsProductReviews.md) |  | [optional] 
**main_categories** | [**\Swagger\Client\Model\ProductRelationshipsMainCategories**](ProductRelationshipsMainCategories.md) |  | [optional] 
**seo_urls** | [**\Swagger\Client\Model\ProductRelationshipsSeoUrls**](ProductRelationshipsSeoUrls.md) |  | [optional] 
**order_line_items** | [**\Swagger\Client\Model\ProductRelationshipsOrderLineItems**](ProductRelationshipsOrderLineItems.md) |  | [optional] 
**options** | [**\Swagger\Client\Model\ProductRelationshipsOptions**](ProductRelationshipsOptions.md) |  | [optional] 
**properties** | [**\Swagger\Client\Model\ProductRelationshipsProperties**](ProductRelationshipsProperties.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\ProductRelationshipsCategories**](ProductRelationshipsCategories.md) |  | [optional] 
**categories_ro** | [**\Swagger\Client\Model\ProductRelationshipsCategoriesRo**](ProductRelationshipsCategoriesRo.md) |  | [optional] 
**tags** | [**\Swagger\Client\Model\ProductRelationshipsTags**](ProductRelationshipsTags.md) |  | [optional] 
**custom_field_sets** | [**\Swagger\Client\Model\ProductRelationshipsCustomFieldSets**](ProductRelationshipsCustomFieldSets.md) |  | [optional] 
**feature_set** | [**\Swagger\Client\Model\ProductRelationshipsFeatureSet**](ProductRelationshipsFeatureSet.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

