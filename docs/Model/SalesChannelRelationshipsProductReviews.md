# SalesChannelRelationshipsProductReviews

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsProductReviewsLinks**](SalesChannelRelationshipsProductReviewsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SalesChannelRelationshipsProductReviewsData[]**](SalesChannelRelationshipsProductReviewsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

