# PromotionPersonaCustomerFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**promotion_id** | **string** |  | 
**customer_id** | **string** |  | 
**promotion** | [**\Swagger\Client\Model\PromotionFlat**](PromotionFlat.md) |  | [optional] 
**customer** | [**\Swagger\Client\Model\CustomerFlat**](CustomerFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

