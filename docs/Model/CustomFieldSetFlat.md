# CustomFieldSetFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**config** | **object** |  | [optional] 
**active** | **bool** |  | [optional] 
**global** | **bool** |  | [optional] 
**position** | **int** |  | [optional] 
**app_id** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**custom_fields** | [**\Swagger\Client\Model\CustomFieldFlat**](CustomFieldFlat.md) |  | [optional] 
**relations** | [**\Swagger\Client\Model\CustomFieldSetRelationFlat**](CustomFieldSetRelationFlat.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\ProductFlat**](ProductFlat.md) |  | [optional] 
**app** | [**\Swagger\Client\Model\AppFlat**](AppFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

