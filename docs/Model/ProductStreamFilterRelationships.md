# ProductStreamFilterRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_stream** | [**\Swagger\Client\Model\ProductStreamFilterRelationshipsProductStream**](ProductStreamFilterRelationshipsProductStream.md) |  | [optional] 
**parent** | [**\Swagger\Client\Model\ProductStreamFilterRelationshipsParent**](ProductStreamFilterRelationshipsParent.md) |  | [optional] 
**queries** | [**\Swagger\Client\Model\ProductStreamFilterRelationshipsQueries**](ProductStreamFilterRelationshipsQueries.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

