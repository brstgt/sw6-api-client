# CustomFieldRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_field_set** | [**\Swagger\Client\Model\CustomFieldRelationshipsCustomFieldSet**](CustomFieldRelationshipsCustomFieldSet.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

