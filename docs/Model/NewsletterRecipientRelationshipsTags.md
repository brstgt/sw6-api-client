# NewsletterRecipientRelationshipsTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsTagsLinks**](NewsletterRecipientRelationshipsTagsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsTagsData[]**](NewsletterRecipientRelationshipsTagsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

