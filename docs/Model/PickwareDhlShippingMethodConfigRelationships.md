# PickwareDhlShippingMethodConfigRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_method** | [**\Swagger\Client\Model\PickwareDhlShippingMethodConfigRelationshipsShippingMethod**](PickwareDhlShippingMethodConfigRelationshipsShippingMethod.md) |  | [optional] 
**carrier** | [**\Swagger\Client\Model\PickwareDhlShippingMethodConfigRelationshipsCarrier**](PickwareDhlShippingMethodConfigRelationshipsCarrier.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

