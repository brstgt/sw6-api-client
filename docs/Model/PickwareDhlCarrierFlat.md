# PickwareDhlCarrierFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**technical_name** | **string** |  | 
**name** | **string** |  | 
**abbreviation** | **string** |  | 
**config_domain** | **string** |  | 
**shipment_config_options** | **object** |  | 
**shipment_config_default_values** | **object** |  | 
**installed** | **bool** |  | 
**capabilities** | **object** |  | [optional] 
**return_label_mail_template_type_technical_name** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**return_label_mail_template_type** | [**\Swagger\Client\Model\MailTemplateTypeFlat**](MailTemplateTypeFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

