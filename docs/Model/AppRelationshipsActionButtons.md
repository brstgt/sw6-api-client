# AppRelationshipsActionButtons

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\AppRelationshipsActionButtonsLinks**](AppRelationshipsActionButtonsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\AppRelationshipsActionButtonsData[]**](AppRelationshipsActionButtonsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

