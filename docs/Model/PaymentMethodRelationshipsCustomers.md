# PaymentMethodRelationshipsCustomers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\PaymentMethodRelationshipsCustomersLinks**](PaymentMethodRelationshipsCustomersLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\PaymentMethodRelationshipsCustomersData[]**](PaymentMethodRelationshipsCustomersData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

