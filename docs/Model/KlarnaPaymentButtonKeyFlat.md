# KlarnaPaymentButtonKeyFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**button_key** | **string** |  | 
**sales_channel_domain_id** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**sales_channel_domain** | [**\Swagger\Client\Model\SalesChannelDomainFlat**](SalesChannelDomainFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

