# SwagMigrationConnectionRelationshipsRuns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsRunsLinks**](SwagMigrationConnectionRelationshipsRunsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsRunsData[]**](SwagMigrationConnectionRelationshipsRunsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

