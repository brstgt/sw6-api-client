# KlarnaPaymentButtonKeyRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channel_domain** | [**\Swagger\Client\Model\KlarnaPaymentButtonKeyRelationshipsSalesChannelDomain**](KlarnaPaymentButtonKeyRelationshipsSalesChannelDomain.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

