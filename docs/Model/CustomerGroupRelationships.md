# CustomerGroupRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**\Swagger\Client\Model\CustomerGroupRelationshipsCustomers**](CustomerGroupRelationshipsCustomers.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\CustomerGroupRelationshipsSalesChannels**](CustomerGroupRelationshipsSalesChannels.md) |  | [optional] 
**registration_sales_channels** | [**\Swagger\Client\Model\CustomerGroupRelationshipsRegistrationSalesChannels**](CustomerGroupRelationshipsRegistrationSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

