# NumberRangeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\NumberRangeRelationshipsType**](NumberRangeRelationshipsType.md) |  | [optional] 
**number_range_sales_channels** | [**\Swagger\Client\Model\NumberRangeRelationshipsNumberRangeSalesChannels**](NumberRangeRelationshipsNumberRangeSalesChannels.md) |  | [optional] 
**state** | [**\Swagger\Client\Model\NumberRangeRelationshipsState**](NumberRangeRelationshipsState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

