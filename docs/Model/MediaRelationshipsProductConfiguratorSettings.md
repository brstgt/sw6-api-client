# MediaRelationshipsProductConfiguratorSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\MediaRelationshipsProductConfiguratorSettingsLinks**](MediaRelationshipsProductConfiguratorSettingsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\MediaRelationshipsProductConfiguratorSettingsData[]**](MediaRelationshipsProductConfiguratorSettingsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

