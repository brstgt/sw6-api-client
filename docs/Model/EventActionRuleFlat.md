# EventActionRuleFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**event_action_id** | **string** |  | 
**rule_id** | **string** |  | 
**event_action** | [**\Swagger\Client\Model\EventActionFlat**](EventActionFlat.md) |  | [optional] 
**rule** | [**\Swagger\Client\Model\RuleFlat**](RuleFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

