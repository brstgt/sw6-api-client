# PromotionDiscountRelationshipsPromotionDiscountPrices

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\PromotionDiscountRelationshipsPromotionDiscountPricesLinks**](PromotionDiscountRelationshipsPromotionDiscountPricesLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\PromotionDiscountRelationshipsPromotionDiscountPricesData[]**](PromotionDiscountRelationshipsPromotionDiscountPricesData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

