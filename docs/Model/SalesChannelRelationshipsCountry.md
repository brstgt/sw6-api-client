# SalesChannelRelationshipsCountry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsCountryLinks**](SalesChannelRelationshipsCountryLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CountryStateRelationshipsCountryData**](CountryStateRelationshipsCountryData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

