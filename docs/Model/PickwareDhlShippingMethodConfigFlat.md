# PickwareDhlShippingMethodConfigFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipping_method_id** | **string** |  | 
**carrier_technical_name** | **string** |  | 
**shipment_config** | **object** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**shipping_method** | [**\Swagger\Client\Model\ShippingMethodFlat**](ShippingMethodFlat.md) |  | [optional] 
**carrier** | [**\Swagger\Client\Model\PickwareDhlCarrierFlat**](PickwareDhlCarrierFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

