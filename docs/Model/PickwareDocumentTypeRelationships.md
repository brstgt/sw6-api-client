# PickwareDocumentTypeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**\Swagger\Client\Model\PickwareDocumentTypeRelationshipsDocuments**](PickwareDocumentTypeRelationshipsDocuments.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

