# TagRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\Swagger\Client\Model\TagRelationshipsProducts**](TagRelationshipsProducts.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\TagRelationshipsMedia**](TagRelationshipsMedia.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\TagRelationshipsCategories**](TagRelationshipsCategories.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\TagRelationshipsCustomers**](TagRelationshipsCustomers.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\TagRelationshipsOrders**](TagRelationshipsOrders.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\TagRelationshipsShippingMethods**](TagRelationshipsShippingMethods.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\TagRelationshipsNewsletterRecipients**](TagRelationshipsNewsletterRecipients.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

