# OrderRelationshipsAddresses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\OrderRelationshipsAddressesLinks**](OrderRelationshipsAddressesLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\OrderRelationshipsAddressesData[]**](OrderRelationshipsAddressesData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

