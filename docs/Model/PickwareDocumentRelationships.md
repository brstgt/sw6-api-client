# PickwareDocumentRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | [**\Swagger\Client\Model\PickwareDocumentRelationshipsDocumentType**](PickwareDocumentRelationshipsDocumentType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

