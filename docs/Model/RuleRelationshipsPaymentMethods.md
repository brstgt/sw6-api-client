# RuleRelationshipsPaymentMethods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\RuleRelationshipsPaymentMethodsLinks**](RuleRelationshipsPaymentMethodsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\RuleRelationshipsPaymentMethodsData[]**](RuleRelationshipsPaymentMethodsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

