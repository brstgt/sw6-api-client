# EventActionSalesChannelFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**event_action_id** | **string** |  | 
**sales_channel_id** | **string** |  | 
**event_action** | [**\Swagger\Client\Model\EventActionFlat**](EventActionFlat.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

