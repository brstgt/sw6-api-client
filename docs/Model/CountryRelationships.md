# CountryRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**states** | [**\Swagger\Client\Model\CountryRelationshipsStates**](CountryRelationshipsStates.md) |  | [optional] 
**customer_addresses** | [**\Swagger\Client\Model\CountryRelationshipsCustomerAddresses**](CountryRelationshipsCustomerAddresses.md) |  | [optional] 
**order_addresses** | [**\Swagger\Client\Model\CountryRelationshipsOrderAddresses**](CountryRelationshipsOrderAddresses.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\CountryRelationshipsSalesChannelDefaultAssignments**](CountryRelationshipsSalesChannelDefaultAssignments.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\CountryRelationshipsSalesChannels**](CountryRelationshipsSalesChannels.md) |  | [optional] 
**tax_rules** | [**\Swagger\Client\Model\CountryRelationshipsTaxRules**](CountryRelationshipsTaxRules.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

