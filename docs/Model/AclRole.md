# AclRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**privileges** | **object[]** |  | 
**relationships** | [**\Swagger\Client\Model\AclRoleRelationships**](AclRoleRelationships.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

