# SalesChannelRelationshipsSystemConfigs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsSystemConfigsLinks**](SalesChannelRelationshipsSystemConfigsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SalesChannelRelationshipsSystemConfigsData[]**](SalesChannelRelationshipsSystemConfigsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

