# OrderCustomerRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | [**\Swagger\Client\Model\OrderCustomerRelationshipsOrder**](OrderCustomerRelationshipsOrder.md) |  | [optional] 
**customer** | [**\Swagger\Client\Model\OrderCustomerRelationshipsCustomer**](OrderCustomerRelationshipsCustomer.md) |  | [optional] 
**salutation** | [**\Swagger\Client\Model\OrderCustomerRelationshipsSalutation**](OrderCustomerRelationshipsSalutation.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

