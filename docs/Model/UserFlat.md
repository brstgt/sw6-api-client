# UserFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**locale_id** | **string** |  | 
**avatar_id** | **string** |  | [optional] 
**username** | **string** |  | 
**first_name** | **string** |  | 
**last_name** | **string** |  | 
**title** | **string** |  | [optional] 
**email** | **string** |  | 
**active** | **bool** |  | [optional] 
**admin** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**locale** | [**\Swagger\Client\Model\LocaleFlat**](LocaleFlat.md) |  | [optional] 
**avatar_media** | [**\Swagger\Client\Model\MediaFlat**](MediaFlat.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\MediaFlat**](MediaFlat.md) |  | [optional] 
**access_keys** | [**\Swagger\Client\Model\UserAccessKeyFlat**](UserAccessKeyFlat.md) |  | [optional] 
**state_machine_history_entries** | [**\Swagger\Client\Model\StateMachineHistoryFlat**](StateMachineHistoryFlat.md) |  | [optional] 
**import_export_log_entries** | [**\Swagger\Client\Model\ImportExportLogFlat**](ImportExportLogFlat.md) |  | [optional] 
**acl_roles** | [**\Swagger\Client\Model\AclRoleFlat**](AclRoleFlat.md) |  | [optional] 
**recovery_user** | [**\Swagger\Client\Model\UserRecoveryFlat**](UserRecoveryFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

