# ProductCrossSellingRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductCrossSellingRelationshipsProduct**](ProductCrossSellingRelationshipsProduct.md) |  | [optional] 
**product_stream** | [**\Swagger\Client\Model\ProductCrossSellingRelationshipsProductStream**](ProductCrossSellingRelationshipsProductStream.md) |  | [optional] 
**assigned_products** | [**\Swagger\Client\Model\ProductCrossSellingRelationshipsAssignedProducts**](ProductCrossSellingRelationshipsAssignedProducts.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

