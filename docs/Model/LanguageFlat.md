# LanguageFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**locale_id** | **string** |  | 
**translation_code_id** | **string** |  | [optional] 
**name** | **string** |  | 
**custom_fields** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**parent** | [**\Swagger\Client\Model\LanguageFlat**](LanguageFlat.md) |  | [optional] 
**locale** | [**\Swagger\Client\Model\LocaleFlat**](LocaleFlat.md) |  | [optional] 
**translation_code** | [**\Swagger\Client\Model\LocaleFlat**](LocaleFlat.md) |  | [optional] 
**children** | [**\Swagger\Client\Model\LanguageFlat**](LanguageFlat.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**sales_channel_domains** | [**\Swagger\Client\Model\SalesChannelDomainFlat**](SalesChannelDomainFlat.md) |  | [optional] 
**customers** | [**\Swagger\Client\Model\CustomerFlat**](CustomerFlat.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\NewsletterRecipientFlat**](NewsletterRecipientFlat.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\OrderFlat**](OrderFlat.md) |  | [optional] 
**product_search_keywords** | [**\Swagger\Client\Model\ProductSearchKeywordFlat**](ProductSearchKeywordFlat.md) |  | [optional] 
**product_keyword_dictionaries** | [**\Swagger\Client\Model\ProductKeywordDictionaryFlat**](ProductKeywordDictionaryFlat.md) |  | [optional] 
**product_reviews** | [**\Swagger\Client\Model\ProductReviewFlat**](ProductReviewFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

