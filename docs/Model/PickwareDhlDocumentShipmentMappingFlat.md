# PickwareDhlDocumentShipmentMappingFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipment_id** | **string** |  | 
**document_id** | **string** |  | 
**shipment** | [**\Swagger\Client\Model\PickwareDhlShipmentFlat**](PickwareDhlShipmentFlat.md) |  | [optional] 
**document** | [**\Swagger\Client\Model\PickwareDocumentFlat**](PickwareDocumentFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

