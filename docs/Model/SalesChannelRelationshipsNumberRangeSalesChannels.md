# SalesChannelRelationshipsNumberRangeSalesChannels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsNumberRangeSalesChannelsLinks**](SalesChannelRelationshipsNumberRangeSalesChannelsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SalesChannelRelationshipsNumberRangeSalesChannelsData[]**](SalesChannelRelationshipsNumberRangeSalesChannelsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

