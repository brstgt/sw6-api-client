# SalesChannelRelationshipsCustomerGroupsRegistrations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SalesChannelRelationshipsCustomerGroupsRegistrationsLinks**](SalesChannelRelationshipsCustomerGroupsRegistrationsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SalesChannelRelationshipsCustomerGroupsRegistrationsData[]**](SalesChannelRelationshipsCustomerGroupsRegistrationsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

