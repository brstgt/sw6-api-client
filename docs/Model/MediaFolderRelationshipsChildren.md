# MediaFolderRelationshipsChildren

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\MediaFolderRelationshipsChildrenLinks**](MediaFolderRelationshipsChildrenLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\MediaFolderRelationshipsChildrenData[]**](MediaFolderRelationshipsChildrenData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

