# ProductPriceRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductPriceRelationshipsProduct**](ProductPriceRelationshipsProduct.md) |  | [optional] 
**rule** | [**\Swagger\Client\Model\ProductPriceRelationshipsRule**](ProductPriceRelationshipsRule.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

