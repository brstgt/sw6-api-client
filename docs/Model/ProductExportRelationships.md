# ProductExportRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_stream** | [**\Swagger\Client\Model\ProductExportRelationshipsProductStream**](ProductExportRelationshipsProductStream.md) |  | [optional] 
**storefront_sales_channel** | [**\Swagger\Client\Model\ProductExportRelationshipsStorefrontSalesChannel**](ProductExportRelationshipsStorefrontSalesChannel.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\ProductExportRelationshipsSalesChannel**](ProductExportRelationshipsSalesChannel.md) |  | [optional] 
**sales_channel_domain** | [**\Swagger\Client\Model\ProductExportRelationshipsSalesChannelDomain**](ProductExportRelationshipsSalesChannelDomain.md) |  | [optional] 
**currency** | [**\Swagger\Client\Model\ProductExportRelationshipsCurrency**](ProductExportRelationshipsCurrency.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

