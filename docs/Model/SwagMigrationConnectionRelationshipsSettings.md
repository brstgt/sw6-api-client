# SwagMigrationConnectionRelationshipsSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsSettingsLinks**](SwagMigrationConnectionRelationshipsSettingsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\SwagMigrationConnectionRelationshipsSettingsData[]**](SwagMigrationConnectionRelationshipsSettingsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

