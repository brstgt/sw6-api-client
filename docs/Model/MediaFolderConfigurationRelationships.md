# MediaFolderConfigurationRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media_folders** | [**\Swagger\Client\Model\MediaFolderConfigurationRelationshipsMediaFolders**](MediaFolderConfigurationRelationshipsMediaFolders.md) |  | [optional] 
**media_thumbnail_sizes** | [**\Swagger\Client\Model\MediaFolderConfigurationRelationshipsMediaThumbnailSizes**](MediaFolderConfigurationRelationshipsMediaThumbnailSizes.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

