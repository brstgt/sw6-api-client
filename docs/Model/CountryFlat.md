# CountryFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**iso** | **string** |  | [optional] 
**position** | **int** |  | [optional] 
**tax_free** | **bool** |  | [optional] 
**active** | **bool** |  | [optional] 
**shipping_available** | **bool** |  | [optional] 
**iso3** | **string** |  | [optional] 
**display_state_in_registration** | **bool** |  | [optional] 
**force_state_in_registration** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**states** | [**\Swagger\Client\Model\CountryStateFlat**](CountryStateFlat.md) |  | [optional] 
**customer_addresses** | [**\Swagger\Client\Model\CustomerAddressFlat**](CustomerAddressFlat.md) |  | [optional] 
**order_addresses** | [**\Swagger\Client\Model\OrderAddressFlat**](OrderAddressFlat.md) |  | [optional] 
**sales_channel_default_assignments** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**sales_channels** | [**\Swagger\Client\Model\SalesChannelFlat**](SalesChannelFlat.md) |  | [optional] 
**tax_rules** | [**\Swagger\Client\Model\TaxRuleFlat**](TaxRuleFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

