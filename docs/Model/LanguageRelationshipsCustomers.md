# LanguageRelationshipsCustomers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\LanguageRelationshipsCustomersLinks**](LanguageRelationshipsCustomersLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\LanguageRelationshipsCustomersData[]**](LanguageRelationshipsCustomersData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

