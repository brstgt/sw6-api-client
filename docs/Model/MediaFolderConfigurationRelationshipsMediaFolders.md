# MediaFolderConfigurationRelationshipsMediaFolders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\MediaFolderConfigurationRelationshipsMediaFoldersLinks**](MediaFolderConfigurationRelationshipsMediaFoldersLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\MediaFolderConfigurationRelationshipsMediaFoldersData[]**](MediaFolderConfigurationRelationshipsMediaFoldersData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

