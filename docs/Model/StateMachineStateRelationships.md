# StateMachineStateRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine** | [**\Swagger\Client\Model\StateMachineStateRelationshipsStateMachine**](StateMachineStateRelationshipsStateMachine.md) |  | [optional] 
**from_state_machine_transitions** | [**\Swagger\Client\Model\StateMachineStateRelationshipsFromStateMachineTransitions**](StateMachineStateRelationshipsFromStateMachineTransitions.md) |  | [optional] 
**to_state_machine_transitions** | [**\Swagger\Client\Model\StateMachineStateRelationshipsToStateMachineTransitions**](StateMachineStateRelationshipsToStateMachineTransitions.md) |  | [optional] 
**order_transactions** | [**\Swagger\Client\Model\StateMachineStateRelationshipsOrderTransactions**](StateMachineStateRelationshipsOrderTransactions.md) |  | [optional] 
**order_deliveries** | [**\Swagger\Client\Model\StateMachineStateRelationshipsOrderDeliveries**](StateMachineStateRelationshipsOrderDeliveries.md) |  | [optional] 
**orders** | [**\Swagger\Client\Model\StateMachineStateRelationshipsOrders**](StateMachineStateRelationshipsOrders.md) |  | [optional] 
**to_state_machine_history_entries** | [**\Swagger\Client\Model\StateMachineStateRelationshipsToStateMachineHistoryEntries**](StateMachineStateRelationshipsToStateMachineHistoryEntries.md) |  | [optional] 
**from_state_machine_history_entries** | [**\Swagger\Client\Model\StateMachineStateRelationshipsFromStateMachineHistoryEntries**](StateMachineStateRelationshipsFromStateMachineHistoryEntries.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

