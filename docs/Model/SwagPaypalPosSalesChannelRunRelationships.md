# SwagPaypalPosSalesChannelRunRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logs** | [**\Swagger\Client\Model\SwagPaypalPosSalesChannelRunRelationshipsLogs**](SwagPaypalPosSalesChannelRunRelationshipsLogs.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

