# DocumentTypeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**\Swagger\Client\Model\DocumentTypeRelationshipsDocuments**](DocumentTypeRelationshipsDocuments.md) |  | [optional] 
**document_base_configs** | [**\Swagger\Client\Model\DocumentTypeRelationshipsDocumentBaseConfigs**](DocumentTypeRelationshipsDocumentBaseConfigs.md) |  | [optional] 
**document_base_config_sales_channels** | [**\Swagger\Client\Model\DocumentTypeRelationshipsDocumentBaseConfigSalesChannels**](DocumentTypeRelationshipsDocumentBaseConfigSalesChannels.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

