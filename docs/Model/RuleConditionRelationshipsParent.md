# RuleConditionRelationshipsParent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\RuleConditionRelationshipsParentLinks**](RuleConditionRelationshipsParentLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\RuleConditionRelationshipsParentData**](RuleConditionRelationshipsParentData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

