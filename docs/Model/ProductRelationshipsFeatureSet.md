# ProductRelationshipsFeatureSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\ProductRelationshipsFeatureSetLinks**](ProductRelationshipsFeatureSetLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\ProductRelationshipsFeatureSetData**](ProductRelationshipsFeatureSetData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

