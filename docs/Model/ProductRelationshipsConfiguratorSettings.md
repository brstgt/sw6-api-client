# ProductRelationshipsConfiguratorSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\ProductRelationshipsConfiguratorSettingsLinks**](ProductRelationshipsConfiguratorSettingsLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\ProductRelationshipsConfiguratorSettingsData[]**](ProductRelationshipsConfiguratorSettingsData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

