# PromotionIndividualCodeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion** | [**\Swagger\Client\Model\PromotionIndividualCodeRelationshipsPromotion**](PromotionIndividualCodeRelationshipsPromotion.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

