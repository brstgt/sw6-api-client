# DocumentRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | [**\Swagger\Client\Model\DocumentRelationshipsDocumentType**](DocumentRelationshipsDocumentType.md) |  | [optional] 
**order** | [**\Swagger\Client\Model\DocumentRelationshipsOrder**](DocumentRelationshipsOrder.md) |  | [optional] 
**referenced_document** | [**\Swagger\Client\Model\DocumentRelationshipsReferencedDocument**](DocumentRelationshipsReferencedDocument.md) |  | [optional] 
**dependent_documents** | [**\Swagger\Client\Model\DocumentRelationshipsDependentDocuments**](DocumentRelationshipsDependentDocuments.md) |  | [optional] 
**document_media_file** | [**\Swagger\Client\Model\DocumentRelationshipsDocumentMediaFile**](DocumentRelationshipsDocumentMediaFile.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

