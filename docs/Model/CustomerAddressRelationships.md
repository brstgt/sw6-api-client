# CustomerAddressRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\Swagger\Client\Model\CustomerAddressRelationshipsCustomer**](CustomerAddressRelationshipsCustomer.md) |  | [optional] 
**country** | [**\Swagger\Client\Model\CustomerAddressRelationshipsCountry**](CustomerAddressRelationshipsCountry.md) |  | [optional] 
**country_state** | [**\Swagger\Client\Model\CustomerAddressRelationshipsCountryState**](CustomerAddressRelationshipsCountryState.md) |  | [optional] 
**salutation** | [**\Swagger\Client\Model\CustomerAddressRelationshipsSalutation**](CustomerAddressRelationshipsSalutation.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

