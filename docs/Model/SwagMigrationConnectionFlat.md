# SwagMigrationConnectionFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**credential_fields** | **object** |  | [optional] 
**premapping** | **object** |  | [optional] 
**profile_name** | **string** |  | 
**gateway_name** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**runs** | [**\Swagger\Client\Model\SwagMigrationRunFlat**](SwagMigrationRunFlat.md) |  | [optional] 
**mappings** | [**\Swagger\Client\Model\SwagMigrationMappingFlat**](SwagMigrationMappingFlat.md) |  | [optional] 
**settings** | [**\Swagger\Client\Model\SwagMigrationGeneralSettingFlat**](SwagMigrationGeneralSettingFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

