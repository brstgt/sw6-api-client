# NewsletterRecipientRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tags** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsTags**](NewsletterRecipientRelationshipsTags.md) |  | [optional] 
**salutation** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsSalutation**](NewsletterRecipientRelationshipsSalutation.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsLanguage**](NewsletterRecipientRelationshipsLanguage.md) |  | [optional] 
**sales_channel** | [**\Swagger\Client\Model\NewsletterRecipientRelationshipsSalesChannel**](NewsletterRecipientRelationshipsSalesChannel.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

