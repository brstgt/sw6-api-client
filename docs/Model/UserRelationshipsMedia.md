# UserRelationshipsMedia

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\UserRelationshipsMediaLinks**](UserRelationshipsMediaLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\UserRelationshipsMediaData[]**](UserRelationshipsMediaData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

