# SalutationRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**\Swagger\Client\Model\SalutationRelationshipsCustomers**](SalutationRelationshipsCustomers.md) |  | [optional] 
**customer_addresses** | [**\Swagger\Client\Model\SalutationRelationshipsCustomerAddresses**](SalutationRelationshipsCustomerAddresses.md) |  | [optional] 
**order_customers** | [**\Swagger\Client\Model\SalutationRelationshipsOrderCustomers**](SalutationRelationshipsOrderCustomers.md) |  | [optional] 
**order_addresses** | [**\Swagger\Client\Model\SalutationRelationshipsOrderAddresses**](SalutationRelationshipsOrderAddresses.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\SalutationRelationshipsNewsletterRecipients**](SalutationRelationshipsNewsletterRecipients.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

