# RuleConditionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | [**\Swagger\Client\Model\RuleConditionRelationshipsRule**](RuleConditionRelationshipsRule.md) |  | [optional] 
**parent** | [**\Swagger\Client\Model\RuleConditionRelationshipsParent**](RuleConditionRelationshipsParent.md) |  | [optional] 
**children** | [**\Swagger\Client\Model\RuleConditionRelationshipsChildren**](RuleConditionRelationshipsChildren.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

