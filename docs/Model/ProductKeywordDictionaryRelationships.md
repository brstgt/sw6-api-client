# ProductKeywordDictionaryRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**\Swagger\Client\Model\ProductKeywordDictionaryRelationshipsLanguage**](ProductKeywordDictionaryRelationshipsLanguage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

