# ProductStreamRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**\Swagger\Client\Model\ProductStreamRelationshipsFilters**](ProductStreamRelationshipsFilters.md) |  | [optional] 
**product_cross_sellings** | [**\Swagger\Client\Model\ProductStreamRelationshipsProductCrossSellings**](ProductStreamRelationshipsProductCrossSellings.md) |  | [optional] 
**product_exports** | [**\Swagger\Client\Model\ProductStreamRelationshipsProductExports**](ProductStreamRelationshipsProductExports.md) |  | [optional] 
**categories** | [**\Swagger\Client\Model\ProductStreamRelationshipsCategories**](ProductStreamRelationshipsCategories.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

