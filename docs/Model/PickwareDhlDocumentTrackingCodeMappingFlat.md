# PickwareDhlDocumentTrackingCodeMappingFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**tracking_code_id** | **string** |  | 
**document_id** | **string** |  | 
**tracking_code** | [**\Swagger\Client\Model\PickwareDhlTrackingCodeFlat**](PickwareDhlTrackingCodeFlat.md) |  | [optional] 
**document** | [**\Swagger\Client\Model\PickwareDocumentFlat**](PickwareDocumentFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

