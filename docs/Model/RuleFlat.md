# RuleFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**priority** | **int** |  | 
**description** | **string** |  | [optional] 
**invalid** | **bool** |  | [optional] 
**custom_fields** | **object** |  | [optional] 
**module_types** | **object** |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**conditions** | [**\Swagger\Client\Model\RuleConditionFlat**](RuleConditionFlat.md) |  | [optional] 
**product_prices** | [**\Swagger\Client\Model\ProductPriceFlat**](ProductPriceFlat.md) |  | [optional] 
**shipping_method_prices** | [**\Swagger\Client\Model\ShippingMethodPriceFlat**](ShippingMethodPriceFlat.md) |  | [optional] 
**shipping_method_price_calculations** | [**\Swagger\Client\Model\ShippingMethodPriceFlat**](ShippingMethodPriceFlat.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\ShippingMethodFlat**](ShippingMethodFlat.md) |  | [optional] 
**payment_methods** | [**\Swagger\Client\Model\PaymentMethodFlat**](PaymentMethodFlat.md) |  | [optional] 
**persona_promotions** | [**\Swagger\Client\Model\PromotionFlat**](PromotionFlat.md) |  | [optional] 
**order_promotions** | [**\Swagger\Client\Model\PromotionFlat**](PromotionFlat.md) |  | [optional] 
**cart_promotions** | [**\Swagger\Client\Model\PromotionFlat**](PromotionFlat.md) |  | [optional] 
**promotion_discounts** | [**\Swagger\Client\Model\PromotionDiscountFlat**](PromotionDiscountFlat.md) |  | [optional] 
**promotion_set_groups** | [**\Swagger\Client\Model\PromotionSetgroupFlat**](PromotionSetgroupFlat.md) |  | [optional] 
**event_actions** | [**\Swagger\Client\Model\EventActionFlat**](EventActionFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

