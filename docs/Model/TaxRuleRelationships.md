# TaxRuleRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\TaxRuleRelationshipsType**](TaxRuleRelationshipsType.md) |  | [optional] 
**country** | [**\Swagger\Client\Model\TaxRuleRelationshipsCountry**](TaxRuleRelationshipsCountry.md) |  | [optional] 
**tax** | [**\Swagger\Client\Model\TaxRuleRelationshipsTax**](TaxRuleRelationshipsTax.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

