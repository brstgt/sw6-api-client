# ProductCrossSellingAssignedProductsRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductCrossSellingAssignedProductsRelationshipsProduct**](ProductCrossSellingAssignedProductsRelationshipsProduct.md) |  | [optional] 
**cross_selling** | [**\Swagger\Client\Model\ProductCrossSellingAssignedProductsRelationshipsCrossSelling**](ProductCrossSellingAssignedProductsRelationshipsCrossSelling.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

