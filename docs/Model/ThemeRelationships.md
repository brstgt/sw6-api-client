# ThemeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales_channels** | [**\Swagger\Client\Model\ThemeRelationshipsSalesChannels**](ThemeRelationshipsSalesChannels.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\ThemeRelationshipsMedia**](ThemeRelationshipsMedia.md) |  | [optional] 
**preview_media** | [**\Swagger\Client\Model\ThemeRelationshipsPreviewMedia**](ThemeRelationshipsPreviewMedia.md) |  | [optional] 
**child_themes** | [**\Swagger\Client\Model\ThemeRelationshipsChildThemes**](ThemeRelationshipsChildThemes.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

