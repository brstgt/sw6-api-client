# PaymentMethodRelationshipsPlugin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\PaymentMethodRelationshipsPluginLinks**](PaymentMethodRelationshipsPluginLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\PaymentMethodRelationshipsPluginData**](PaymentMethodRelationshipsPluginData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

