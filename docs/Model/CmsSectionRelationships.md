# CmsSectionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | [**\Swagger\Client\Model\CmsSectionRelationshipsPage**](CmsSectionRelationshipsPage.md) |  | [optional] 
**background_media** | [**\Swagger\Client\Model\CmsSectionRelationshipsBackgroundMedia**](CmsSectionRelationshipsBackgroundMedia.md) |  | [optional] 
**blocks** | [**\Swagger\Client\Model\CmsSectionRelationshipsBlocks**](CmsSectionRelationshipsBlocks.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

