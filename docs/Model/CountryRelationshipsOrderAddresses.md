# CountryRelationshipsOrderAddresses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CountryRelationshipsOrderAddressesLinks**](CountryRelationshipsOrderAddressesLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CountryRelationshipsOrderAddressesData[]**](CountryRelationshipsOrderAddressesData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

