# SalutationFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**salutation_key** | **string** |  | 
**display_name** | **string** |  | 
**letter_name** | **string** |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | [optional] 
**translated** | **object** |  | [optional] 
**customers** | [**\Swagger\Client\Model\CustomerFlat**](CustomerFlat.md) |  | [optional] 
**customer_addresses** | [**\Swagger\Client\Model\CustomerAddressFlat**](CustomerAddressFlat.md) |  | [optional] 
**order_customers** | [**\Swagger\Client\Model\OrderCustomerFlat**](OrderCustomerFlat.md) |  | [optional] 
**order_addresses** | [**\Swagger\Client\Model\OrderAddressFlat**](OrderAddressFlat.md) |  | [optional] 
**newsletter_recipients** | [**\Swagger\Client\Model\NewsletterRecipientFlat**](NewsletterRecipientFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

