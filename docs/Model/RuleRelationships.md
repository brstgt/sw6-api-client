# RuleRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conditions** | [**\Swagger\Client\Model\RuleRelationshipsConditions**](RuleRelationshipsConditions.md) |  | [optional] 
**product_prices** | [**\Swagger\Client\Model\RuleRelationshipsProductPrices**](RuleRelationshipsProductPrices.md) |  | [optional] 
**shipping_method_prices** | [**\Swagger\Client\Model\RuleRelationshipsShippingMethodPrices**](RuleRelationshipsShippingMethodPrices.md) |  | [optional] 
**shipping_method_price_calculations** | [**\Swagger\Client\Model\RuleRelationshipsShippingMethodPriceCalculations**](RuleRelationshipsShippingMethodPriceCalculations.md) |  | [optional] 
**shipping_methods** | [**\Swagger\Client\Model\RuleRelationshipsShippingMethods**](RuleRelationshipsShippingMethods.md) |  | [optional] 
**payment_methods** | [**\Swagger\Client\Model\RuleRelationshipsPaymentMethods**](RuleRelationshipsPaymentMethods.md) |  | [optional] 
**persona_promotions** | [**\Swagger\Client\Model\RuleRelationshipsPersonaPromotions**](RuleRelationshipsPersonaPromotions.md) |  | [optional] 
**order_promotions** | [**\Swagger\Client\Model\RuleRelationshipsOrderPromotions**](RuleRelationshipsOrderPromotions.md) |  | [optional] 
**cart_promotions** | [**\Swagger\Client\Model\RuleRelationshipsCartPromotions**](RuleRelationshipsCartPromotions.md) |  | [optional] 
**promotion_discounts** | [**\Swagger\Client\Model\RuleRelationshipsPromotionDiscounts**](RuleRelationshipsPromotionDiscounts.md) |  | [optional] 
**promotion_set_groups** | [**\Swagger\Client\Model\RuleRelationshipsPromotionSetGroups**](RuleRelationshipsPromotionSetGroups.md) |  | [optional] 
**event_actions** | [**\Swagger\Client\Model\RuleRelationshipsEventActions**](RuleRelationshipsEventActions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

