# ProductSearchKeywordRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Swagger\Client\Model\ProductSearchKeywordRelationshipsProduct**](ProductSearchKeywordRelationshipsProduct.md) |  | [optional] 
**language** | [**\Swagger\Client\Model\ProductSearchKeywordRelationshipsLanguage**](ProductSearchKeywordRelationshipsLanguage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

