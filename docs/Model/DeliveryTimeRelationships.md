# DeliveryTimeRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_methods** | [**\Swagger\Client\Model\DeliveryTimeRelationshipsShippingMethods**](DeliveryTimeRelationshipsShippingMethods.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\DeliveryTimeRelationshipsProducts**](DeliveryTimeRelationshipsProducts.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

