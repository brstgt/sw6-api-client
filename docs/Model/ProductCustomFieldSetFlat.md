# ProductCustomFieldSetFlat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**product_id** | **string** |  | 
**custom_field_set_id** | **string** |  | 
**product_version_id** | **string** |  | [optional] 
**product** | [**\Swagger\Client\Model\ProductFlat**](ProductFlat.md) |  | [optional] 
**custom_field_set** | [**\Swagger\Client\Model\CustomFieldSetFlat**](CustomFieldSetFlat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

