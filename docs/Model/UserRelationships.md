# UserRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | [**\Swagger\Client\Model\UserRelationshipsLocale**](UserRelationshipsLocale.md) |  | [optional] 
**avatar_media** | [**\Swagger\Client\Model\UserRelationshipsAvatarMedia**](UserRelationshipsAvatarMedia.md) |  | [optional] 
**media** | [**\Swagger\Client\Model\UserRelationshipsMedia**](UserRelationshipsMedia.md) |  | [optional] 
**access_keys** | [**\Swagger\Client\Model\UserRelationshipsAccessKeys**](UserRelationshipsAccessKeys.md) |  | [optional] 
**state_machine_history_entries** | [**\Swagger\Client\Model\UserRelationshipsStateMachineHistoryEntries**](UserRelationshipsStateMachineHistoryEntries.md) |  | [optional] 
**import_export_log_entries** | [**\Swagger\Client\Model\UserRelationshipsImportExportLogEntries**](UserRelationshipsImportExportLogEntries.md) |  | [optional] 
**acl_roles** | [**\Swagger\Client\Model\UserRelationshipsAclRoles**](UserRelationshipsAclRoles.md) |  | [optional] 
**recovery_user** | [**\Swagger\Client\Model\UserRelationshipsRecoveryUser**](UserRelationshipsRecoveryUser.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

