# MediaDefaultFolderRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**folder** | [**\Swagger\Client\Model\MediaDefaultFolderRelationshipsFolder**](MediaDefaultFolderRelationshipsFolder.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

