# OrderTransactionRelationships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_machine_state** | [**\Swagger\Client\Model\OrderTransactionRelationshipsStateMachineState**](OrderTransactionRelationshipsStateMachineState.md) |  | [optional] 
**order** | [**\Swagger\Client\Model\OrderTransactionRelationshipsOrder**](OrderTransactionRelationshipsOrder.md) |  | [optional] 
**payment_method** | [**\Swagger\Client\Model\OrderTransactionRelationshipsPaymentMethod**](OrderTransactionRelationshipsPaymentMethod.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

