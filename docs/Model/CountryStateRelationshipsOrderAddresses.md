# CountryStateRelationshipsOrderAddresses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\Swagger\Client\Model\CountryStateRelationshipsOrderAddressesLinks**](CountryStateRelationshipsOrderAddressesLinks.md) |  | [optional] 
**data** | [**\Swagger\Client\Model\CountryStateRelationshipsOrderAddressesData[]**](CountryStateRelationshipsOrderAddressesData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

