# Swagger\Client\PickwareDhlCarrierApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPickwareDhlCarrier**](PickwareDhlCarrierApi.md#createpickwaredhlcarrier) | **POST** /pickware-dhl-carrier | Create a new Pickware Dhl Carrier resources
[**deletePickwareDhlCarrier**](PickwareDhlCarrierApi.md#deletepickwaredhlcarrier) | **DELETE** /pickware-dhl-carrier/{id} | Delete a Pickware Dhl Carrier resource
[**getPickwareDhlCarrier**](PickwareDhlCarrierApi.md#getpickwaredhlcarrier) | **GET** /pickware-dhl-carrier/{id} | Detailed information about a Pickware Dhl Carrier resource
[**getPickwareDhlCarrierList**](PickwareDhlCarrierApi.md#getpickwaredhlcarrierlist) | **GET** /pickware-dhl-carrier | List with basic information of Pickware Dhl Carrier resources
[**updatePickwareDhlCarrier**](PickwareDhlCarrierApi.md#updatepickwaredhlcarrier) | **PATCH** /pickware-dhl-carrier/{id} | Partially update information about a Pickware Dhl Carrier resource

# **createPickwareDhlCarrier**
> \Swagger\Client\Model\InlineResponse200182 createPickwareDhlCarrier($body, $_response)

Create a new Pickware Dhl Carrier resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body123(); // \Swagger\Client\Model\Body123 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlCarrier($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->createPickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body123**](../Model/Body123.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200182**](../Model/InlineResponse200182.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPickwareDhlCarrier**
> \Swagger\Client\Model\InlineResponse200182 createPickwareDhlCarrier($body, $_response)

Create a new Pickware Dhl Carrier resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body123(); // \Swagger\Client\Model\Body123 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlCarrier($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->createPickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body123**](../Model/Body123.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200182**](../Model/InlineResponse200182.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePickwareDhlCarrier**
> deletePickwareDhlCarrier($id, $_response)

Delete a Pickware Dhl Carrier resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_carrier
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deletePickwareDhlCarrier($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->deletePickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_carrier |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlCarrier**
> \Swagger\Client\Model\InlineResponse200182 getPickwareDhlCarrier($id)

Detailed information about a Pickware Dhl Carrier resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_carrier

try {
    $result = $apiInstance->getPickwareDhlCarrier($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->getPickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_carrier |

### Return type

[**\Swagger\Client\Model\InlineResponse200182**](../Model/InlineResponse200182.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlCarrierList**
> \Swagger\Client\Model\InlineResponse200180 getPickwareDhlCarrierList($limit, $page, $query)

List with basic information of Pickware Dhl Carrier resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getPickwareDhlCarrierList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->getPickwareDhlCarrierList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200180**](../Model/InlineResponse200180.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlCarrier**
> \Swagger\Client\Model\InlineResponse200182 updatePickwareDhlCarrier($id, $body, $_response)

Partially update information about a Pickware Dhl Carrier resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_carrier
$body = new \Swagger\Client\Model\Body124(); // \Swagger\Client\Model\Body124 | Partially update information about a Pickware Dhl Carrier resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlCarrier($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->updatePickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_carrier |
 **body** | [**\Swagger\Client\Model\Body124**](../Model/Body124.md)| Partially update information about a Pickware Dhl Carrier resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200182**](../Model/InlineResponse200182.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlCarrier**
> \Swagger\Client\Model\InlineResponse200182 updatePickwareDhlCarrier($id, $body, $_response)

Partially update information about a Pickware Dhl Carrier resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlCarrierApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_carrier
$body = new \Swagger\Client\Model\Body124(); // \Swagger\Client\Model\Body124 | Partially update information about a Pickware Dhl Carrier resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlCarrier($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlCarrierApi->updatePickwareDhlCarrier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_carrier |
 **body** | [**\Swagger\Client\Model\Body124**](../Model/Body124.md)| Partially update information about a Pickware Dhl Carrier resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200182**](../Model/InlineResponse200182.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

