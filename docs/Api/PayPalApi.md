# Swagger\Client\PayPalApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorizationDetails**](PayPalApi.md#authorizationdetails) | **GET** /paypal-v2/authorization/{orderTransactionId}/{authorizationId} | 
[**captureAuthorization**](PayPalApi.md#captureauthorization) | **POST** /_action/paypal-v2/capture-authorization/{orderTransactionId}/{authorizationId} | 
[**captureDetails**](PayPalApi.md#capturedetails) | **GET** /paypal-v2/capture/{orderTransactionId}/{captureId} | 
[**orderDetails**](PayPalApi.md#orderdetails) | **GET** /paypal-v2/order/{orderTransactionId}/{paypalOrderId} | 
[**paymentDetails**](PayPalApi.md#paymentdetails) | **GET** /paypal/payment-details/{orderId}/{paymentId} | 
[**refundCapture**](PayPalApi.md#refundcapture) | **POST** /_action/paypal-v2/refund-capture/{orderTransactionId}/{captureId}/{paypalOrderId} | 
[**refundDetails**](PayPalApi.md#refunddetails) | **GET** /paypal-v2/refund/{orderTransactionId}/{refundId} | 
[**resourceDetails**](PayPalApi.md#resourcedetails) | **GET** /paypal/resource-details/{resourceType}/{resourceId}/{orderId} | 
[**voidAuthorization**](PayPalApi.md#voidauthorization) | **POST** /_action/paypal-v2/void-authorization/{orderTransactionId}/{authorizationId} | 

# **authorizationDetails**
> array authorizationDetails($order_transaction_id, $authorization_id)



Loads the authorization details of the given PayPal authorization ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$authorization_id = "authorization_id_example"; // string | ID of the PayPal authorization

try {
    $result = $apiInstance->authorizationDetails($order_transaction_id, $authorization_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->authorizationDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **authorization_id** | **string**| ID of the PayPal authorization |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **captureAuthorization**
> array captureAuthorization($order_transaction_id, $authorization_id, $body)



Captures the PayPal authorization and sets the state of the Shopware order transaction accordingly

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$authorization_id = "authorization_id_example"; // string | ID of the PayPal authorization
$body = new \Swagger\Client\Model\Body1(); // \Swagger\Client\Model\Body1 | 

try {
    $result = $apiInstance->captureAuthorization($order_transaction_id, $authorization_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->captureAuthorization: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **authorization_id** | **string**| ID of the PayPal authorization |
 **body** | [**\Swagger\Client\Model\Body1**](../Model/Body1.md)|  | [optional]

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **captureDetails**
> array captureDetails($order_transaction_id, $capture_id)



Loads the capture details of the given PayPal capture ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$capture_id = "capture_id_example"; // string | ID of the PayPal capture

try {
    $result = $apiInstance->captureDetails($order_transaction_id, $capture_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->captureDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **capture_id** | **string**| ID of the PayPal capture |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderDetails**
> array orderDetails($order_transaction_id, $paypal_order_id)



Loads the order details of the given PayPal order ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$paypal_order_id = "paypal_order_id_example"; // string | ID of the PayPal order

try {
    $result = $apiInstance->orderDetails($order_transaction_id, $paypal_order_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->orderDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **paypal_order_id** | **string**| ID of the PayPal order |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentDetails**
> array paymentDetails($order_id, $payment_id)



Loads the Payment details of the given PayPal ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_id = "order_id_example"; // string | ID of the order which contains the PayPal payment
$payment_id = "payment_id_example"; // string | ID of the PayPal payment

try {
    $result = $apiInstance->paymentDetails($order_id, $payment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->paymentDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| ID of the order which contains the PayPal payment |
 **payment_id** | **string**| ID of the PayPal payment |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refundCapture**
> array refundCapture($order_transaction_id, $capture_id, $paypal_order_id, $body)



Refunds the PayPal capture and sets the state of the Shopware order transaction accordingly

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$capture_id = "capture_id_example"; // string | ID of the PayPal capture
$paypal_order_id = "paypal_order_id_example"; // string | ID of the PayPal order
$body = new \Swagger\Client\Model\Body(); // \Swagger\Client\Model\Body | 

try {
    $result = $apiInstance->refundCapture($order_transaction_id, $capture_id, $paypal_order_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->refundCapture: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **capture_id** | **string**| ID of the PayPal capture |
 **paypal_order_id** | **string**| ID of the PayPal order |
 **body** | [**\Swagger\Client\Model\Body**](../Model/Body.md)|  | [optional]

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refundDetails**
> array refundDetails($order_transaction_id, $refund_id)



Loads the refund details of the given PayPal refund ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$refund_id = "refund_id_example"; // string | ID of the PayPal refund

try {
    $result = $apiInstance->refundDetails($order_transaction_id, $refund_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->refundDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **refund_id** | **string**| ID of the PayPal refund |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resourceDetails**
> array resourceDetails($resource_type, $resource_id, $order_id)



Loads the PayPal resource details of the given resource ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$resource_type = "resource_type_example"; // string | Type of the resource. Possible values: sale, authorization, order, capture, refund
$resource_id = "resource_id_example"; // string | ID of the PayPal resource
$order_id = "order_id_example"; // string | ID of the order which contains the PayPal resource

try {
    $result = $apiInstance->resourceDetails($resource_type, $resource_id, $order_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->resourceDetails: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resource_type** | **string**| Type of the resource. Possible values: sale, authorization, order, capture, refund |
 **resource_id** | **string**| ID of the PayPal resource |
 **order_id** | **string**| ID of the order which contains the PayPal resource |

### Return type

[**array**](../Model/array.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **voidAuthorization**
> voidAuthorization($order_transaction_id, $authorization_id, $body)



Voids the PayPal authorization and sets the state of the Shopware order transaction accordingly

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PayPalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_transaction_id = "order_transaction_id_example"; // string | ID of the order transaction which contains the PayPal payment
$authorization_id = "authorization_id_example"; // string | ID of the PayPal authorization
$body = new \Swagger\Client\Model\Body2(); // \Swagger\Client\Model\Body2 | 

try {
    $apiInstance->voidAuthorization($order_transaction_id, $authorization_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling PayPalApi->voidAuthorization: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_transaction_id** | **string**| ID of the order transaction which contains the PayPal payment |
 **authorization_id** | **string**| ID of the PayPal authorization |
 **body** | [**\Swagger\Client\Model\Body2**](../Model/Body2.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

