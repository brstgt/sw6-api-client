# Swagger\Client\PickwareDhlShippingMethodConfigApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPickwareDhlShippingMethodConfig**](PickwareDhlShippingMethodConfigApi.md#createpickwaredhlshippingmethodconfig) | **POST** /pickware-dhl-shipping-method-config | Create a new Pickware Dhl Shipping Method Config resources
[**deletePickwareDhlShippingMethodConfig**](PickwareDhlShippingMethodConfigApi.md#deletepickwaredhlshippingmethodconfig) | **DELETE** /pickware-dhl-shipping-method-config/{id} | Delete a Pickware Dhl Shipping Method Config resource
[**getPickwareDhlShippingMethodConfig**](PickwareDhlShippingMethodConfigApi.md#getpickwaredhlshippingmethodconfig) | **GET** /pickware-dhl-shipping-method-config/{id} | Detailed information about a Pickware Dhl Shipping Method Config resource
[**getPickwareDhlShippingMethodConfigList**](PickwareDhlShippingMethodConfigApi.md#getpickwaredhlshippingmethodconfiglist) | **GET** /pickware-dhl-shipping-method-config | List with basic information of Pickware Dhl Shipping Method Config resources
[**updatePickwareDhlShippingMethodConfig**](PickwareDhlShippingMethodConfigApi.md#updatepickwaredhlshippingmethodconfig) | **PATCH** /pickware-dhl-shipping-method-config/{id} | Partially update information about a Pickware Dhl Shipping Method Config resource

# **createPickwareDhlShippingMethodConfig**
> \Swagger\Client\Model\InlineResponse200188 createPickwareDhlShippingMethodConfig($body, $_response)

Create a new Pickware Dhl Shipping Method Config resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body127(); // \Swagger\Client\Model\Body127 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlShippingMethodConfig($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->createPickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body127**](../Model/Body127.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200188**](../Model/InlineResponse200188.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPickwareDhlShippingMethodConfig**
> \Swagger\Client\Model\InlineResponse200188 createPickwareDhlShippingMethodConfig($body, $_response)

Create a new Pickware Dhl Shipping Method Config resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body127(); // \Swagger\Client\Model\Body127 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlShippingMethodConfig($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->createPickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body127**](../Model/Body127.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200188**](../Model/InlineResponse200188.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePickwareDhlShippingMethodConfig**
> deletePickwareDhlShippingMethodConfig($id, $_response)

Delete a Pickware Dhl Shipping Method Config resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipping_method_config
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deletePickwareDhlShippingMethodConfig($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->deletePickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipping_method_config |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlShippingMethodConfig**
> \Swagger\Client\Model\InlineResponse200188 getPickwareDhlShippingMethodConfig($id)

Detailed information about a Pickware Dhl Shipping Method Config resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipping_method_config

try {
    $result = $apiInstance->getPickwareDhlShippingMethodConfig($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->getPickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipping_method_config |

### Return type

[**\Swagger\Client\Model\InlineResponse200188**](../Model/InlineResponse200188.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlShippingMethodConfigList**
> \Swagger\Client\Model\InlineResponse200186 getPickwareDhlShippingMethodConfigList($limit, $page, $query)

List with basic information of Pickware Dhl Shipping Method Config resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getPickwareDhlShippingMethodConfigList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->getPickwareDhlShippingMethodConfigList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200186**](../Model/InlineResponse200186.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlShippingMethodConfig**
> \Swagger\Client\Model\InlineResponse200188 updatePickwareDhlShippingMethodConfig($id, $body, $_response)

Partially update information about a Pickware Dhl Shipping Method Config resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipping_method_config
$body = new \Swagger\Client\Model\Body128(); // \Swagger\Client\Model\Body128 | Partially update information about a Pickware Dhl Shipping Method Config resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlShippingMethodConfig($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->updatePickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipping_method_config |
 **body** | [**\Swagger\Client\Model\Body128**](../Model/Body128.md)| Partially update information about a Pickware Dhl Shipping Method Config resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200188**](../Model/InlineResponse200188.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlShippingMethodConfig**
> \Swagger\Client\Model\InlineResponse200188 updatePickwareDhlShippingMethodConfig($id, $body, $_response)

Partially update information about a Pickware Dhl Shipping Method Config resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShippingMethodConfigApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipping_method_config
$body = new \Swagger\Client\Model\Body128(); // \Swagger\Client\Model\Body128 | Partially update information about a Pickware Dhl Shipping Method Config resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlShippingMethodConfig($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShippingMethodConfigApi->updatePickwareDhlShippingMethodConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipping_method_config |
 **body** | [**\Swagger\Client\Model\Body128**](../Model/Body128.md)| Partially update information about a Pickware Dhl Shipping Method Config resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200188**](../Model/InlineResponse200188.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

