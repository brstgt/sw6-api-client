# Swagger\Client\PickwareDhlShipmentApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPickwareDhlShipment**](PickwareDhlShipmentApi.md#createpickwaredhlshipment) | **POST** /pickware-dhl-shipment | Create a new Pickware Dhl Shipment resources
[**deletePickwareDhlShipment**](PickwareDhlShipmentApi.md#deletepickwaredhlshipment) | **DELETE** /pickware-dhl-shipment/{id} | Delete a Pickware Dhl Shipment resource
[**getPickwareDhlShipment**](PickwareDhlShipmentApi.md#getpickwaredhlshipment) | **GET** /pickware-dhl-shipment/{id} | Detailed information about a Pickware Dhl Shipment resource
[**getPickwareDhlShipmentList**](PickwareDhlShipmentApi.md#getpickwaredhlshipmentlist) | **GET** /pickware-dhl-shipment | List with basic information of Pickware Dhl Shipment resources
[**updatePickwareDhlShipment**](PickwareDhlShipmentApi.md#updatepickwaredhlshipment) | **PATCH** /pickware-dhl-shipment/{id} | Partially update information about a Pickware Dhl Shipment resource

# **createPickwareDhlShipment**
> \Swagger\Client\Model\InlineResponse200185 createPickwareDhlShipment($body, $_response)

Create a new Pickware Dhl Shipment resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body125(); // \Swagger\Client\Model\Body125 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlShipment($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->createPickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body125**](../Model/Body125.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200185**](../Model/InlineResponse200185.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPickwareDhlShipment**
> \Swagger\Client\Model\InlineResponse200185 createPickwareDhlShipment($body, $_response)

Create a new Pickware Dhl Shipment resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body125(); // \Swagger\Client\Model\Body125 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlShipment($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->createPickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body125**](../Model/Body125.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200185**](../Model/InlineResponse200185.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePickwareDhlShipment**
> deletePickwareDhlShipment($id, $_response)

Delete a Pickware Dhl Shipment resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipment
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deletePickwareDhlShipment($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->deletePickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipment |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlShipment**
> \Swagger\Client\Model\InlineResponse200185 getPickwareDhlShipment($id)

Detailed information about a Pickware Dhl Shipment resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipment

try {
    $result = $apiInstance->getPickwareDhlShipment($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->getPickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipment |

### Return type

[**\Swagger\Client\Model\InlineResponse200185**](../Model/InlineResponse200185.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlShipmentList**
> \Swagger\Client\Model\InlineResponse200183 getPickwareDhlShipmentList($limit, $page, $query)

List with basic information of Pickware Dhl Shipment resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getPickwareDhlShipmentList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->getPickwareDhlShipmentList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200183**](../Model/InlineResponse200183.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlShipment**
> \Swagger\Client\Model\InlineResponse200185 updatePickwareDhlShipment($id, $body, $_response)

Partially update information about a Pickware Dhl Shipment resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipment
$body = new \Swagger\Client\Model\Body126(); // \Swagger\Client\Model\Body126 | Partially update information about a Pickware Dhl Shipment resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlShipment($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->updatePickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipment |
 **body** | [**\Swagger\Client\Model\Body126**](../Model/Body126.md)| Partially update information about a Pickware Dhl Shipment resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200185**](../Model/InlineResponse200185.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlShipment**
> \Swagger\Client\Model\InlineResponse200185 updatePickwareDhlShipment($id, $body, $_response)

Partially update information about a Pickware Dhl Shipment resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlShipmentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_shipment
$body = new \Swagger\Client\Model\Body126(); // \Swagger\Client\Model\Body126 | Partially update information about a Pickware Dhl Shipment resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlShipment($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlShipmentApi->updatePickwareDhlShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_shipment |
 **body** | [**\Swagger\Client\Model\Body126**](../Model/Body126.md)| Partially update information about a Pickware Dhl Shipment resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200185**](../Model/InlineResponse200185.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

