# Swagger\Client\KlarnaPaymentRequestLogApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createKlarnaPaymentRequestLog**](KlarnaPaymentRequestLogApi.md#createklarnapaymentrequestlog) | **POST** /klarna-payment-request-log | Create a new Klarna Payment Request Log resources
[**deleteKlarnaPaymentRequestLog**](KlarnaPaymentRequestLogApi.md#deleteklarnapaymentrequestlog) | **DELETE** /klarna-payment-request-log/{id} | Delete a Klarna Payment Request Log resource
[**getKlarnaPaymentRequestLog**](KlarnaPaymentRequestLogApi.md#getklarnapaymentrequestlog) | **GET** /klarna-payment-request-log/{id} | Detailed information about a Klarna Payment Request Log resource
[**getKlarnaPaymentRequestLogList**](KlarnaPaymentRequestLogApi.md#getklarnapaymentrequestloglist) | **GET** /klarna-payment-request-log | List with basic information of Klarna Payment Request Log resources
[**updateKlarnaPaymentRequestLog**](KlarnaPaymentRequestLogApi.md#updateklarnapaymentrequestlog) | **PATCH** /klarna-payment-request-log/{id} | Partially update information about a Klarna Payment Request Log resource

# **createKlarnaPaymentRequestLog**
> \Swagger\Client\Model\InlineResponse20095 createKlarnaPaymentRequestLog($body, $_response)

Create a new Klarna Payment Request Log resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body65(); // \Swagger\Client\Model\Body65 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createKlarnaPaymentRequestLog($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->createKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body65**](../Model/Body65.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createKlarnaPaymentRequestLog**
> \Swagger\Client\Model\InlineResponse20095 createKlarnaPaymentRequestLog($body, $_response)

Create a new Klarna Payment Request Log resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body65(); // \Swagger\Client\Model\Body65 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createKlarnaPaymentRequestLog($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->createKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body65**](../Model/Body65.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteKlarnaPaymentRequestLog**
> deleteKlarnaPaymentRequestLog($id, $_response)

Delete a Klarna Payment Request Log resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_request_log
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deleteKlarnaPaymentRequestLog($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->deleteKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_request_log |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKlarnaPaymentRequestLog**
> \Swagger\Client\Model\InlineResponse20095 getKlarnaPaymentRequestLog($id)

Detailed information about a Klarna Payment Request Log resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_request_log

try {
    $result = $apiInstance->getKlarnaPaymentRequestLog($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->getKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_request_log |

### Return type

[**\Swagger\Client\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKlarnaPaymentRequestLogList**
> \Swagger\Client\Model\InlineResponse20093 getKlarnaPaymentRequestLogList($limit, $page, $query)

List with basic information of Klarna Payment Request Log resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getKlarnaPaymentRequestLogList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->getKlarnaPaymentRequestLogList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20093**](../Model/InlineResponse20093.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKlarnaPaymentRequestLog**
> \Swagger\Client\Model\InlineResponse20095 updateKlarnaPaymentRequestLog($id, $body, $_response)

Partially update information about a Klarna Payment Request Log resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_request_log
$body = new \Swagger\Client\Model\Body66(); // \Swagger\Client\Model\Body66 | Partially update information about a Klarna Payment Request Log resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateKlarnaPaymentRequestLog($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->updateKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_request_log |
 **body** | [**\Swagger\Client\Model\Body66**](../Model/Body66.md)| Partially update information about a Klarna Payment Request Log resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKlarnaPaymentRequestLog**
> \Swagger\Client\Model\InlineResponse20095 updateKlarnaPaymentRequestLog($id, $body, $_response)

Partially update information about a Klarna Payment Request Log resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentRequestLogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_request_log
$body = new \Swagger\Client\Model\Body66(); // \Swagger\Client\Model\Body66 | Partially update information about a Klarna Payment Request Log resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateKlarnaPaymentRequestLog($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentRequestLogApi->updateKlarnaPaymentRequestLog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_request_log |
 **body** | [**\Swagger\Client\Model\Body66**](../Model/Body66.md)| Partially update information about a Klarna Payment Request Log resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20095**](../Model/InlineResponse20095.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

