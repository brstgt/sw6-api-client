# Swagger\Client\RepertusGiftWrapApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRepertusGiftWrap**](RepertusGiftWrapApi.md#createrepertusgiftwrap) | **POST** /repertus-gift-wrap | Create a new Repertus Gift Wrap resources
[**deleteRepertusGiftWrap**](RepertusGiftWrapApi.md#deleterepertusgiftwrap) | **DELETE** /repertus-gift-wrap/{id} | Delete a Repertus Gift Wrap resource
[**getRepertusGiftWrap**](RepertusGiftWrapApi.md#getrepertusgiftwrap) | **GET** /repertus-gift-wrap/{id} | Detailed information about a Repertus Gift Wrap resource
[**getRepertusGiftWrapList**](RepertusGiftWrapApi.md#getrepertusgiftwraplist) | **GET** /repertus-gift-wrap | List with basic information of Repertus Gift Wrap resources
[**updateRepertusGiftWrap**](RepertusGiftWrapApi.md#updaterepertusgiftwrap) | **PATCH** /repertus-gift-wrap/{id} | Partially update information about a Repertus Gift Wrap resource

# **createRepertusGiftWrap**
> \Swagger\Client\Model\InlineResponse200275 createRepertusGiftWrap($body, $_response)

Create a new Repertus Gift Wrap resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body185(); // \Swagger\Client\Model\Body185 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createRepertusGiftWrap($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->createRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body185**](../Model/Body185.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200275**](../Model/InlineResponse200275.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createRepertusGiftWrap**
> \Swagger\Client\Model\InlineResponse200275 createRepertusGiftWrap($body, $_response)

Create a new Repertus Gift Wrap resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body185(); // \Swagger\Client\Model\Body185 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createRepertusGiftWrap($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->createRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body185**](../Model/Body185.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200275**](../Model/InlineResponse200275.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRepertusGiftWrap**
> deleteRepertusGiftWrap($id, $_response)

Delete a Repertus Gift Wrap resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the repertus_gift_wrap
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deleteRepertusGiftWrap($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->deleteRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the repertus_gift_wrap |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRepertusGiftWrap**
> \Swagger\Client\Model\InlineResponse200275 getRepertusGiftWrap($id)

Detailed information about a Repertus Gift Wrap resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the repertus_gift_wrap

try {
    $result = $apiInstance->getRepertusGiftWrap($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->getRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the repertus_gift_wrap |

### Return type

[**\Swagger\Client\Model\InlineResponse200275**](../Model/InlineResponse200275.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRepertusGiftWrapList**
> \Swagger\Client\Model\InlineResponse200273 getRepertusGiftWrapList($limit, $page, $query)

List with basic information of Repertus Gift Wrap resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getRepertusGiftWrapList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->getRepertusGiftWrapList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200273**](../Model/InlineResponse200273.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRepertusGiftWrap**
> \Swagger\Client\Model\InlineResponse200275 updateRepertusGiftWrap($id, $body, $_response)

Partially update information about a Repertus Gift Wrap resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the repertus_gift_wrap
$body = new \Swagger\Client\Model\Body186(); // \Swagger\Client\Model\Body186 | Partially update information about a Repertus Gift Wrap resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateRepertusGiftWrap($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->updateRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the repertus_gift_wrap |
 **body** | [**\Swagger\Client\Model\Body186**](../Model/Body186.md)| Partially update information about a Repertus Gift Wrap resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200275**](../Model/InlineResponse200275.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRepertusGiftWrap**
> \Swagger\Client\Model\InlineResponse200275 updateRepertusGiftWrap($id, $body, $_response)

Partially update information about a Repertus Gift Wrap resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\RepertusGiftWrapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the repertus_gift_wrap
$body = new \Swagger\Client\Model\Body186(); // \Swagger\Client\Model\Body186 | Partially update information about a Repertus Gift Wrap resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateRepertusGiftWrap($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RepertusGiftWrapApi->updateRepertusGiftWrap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the repertus_gift_wrap |
 **body** | [**\Swagger\Client\Model\Body186**](../Model/Body186.md)| Partially update information about a Repertus Gift Wrap resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200275**](../Model/InlineResponse200275.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

