# Swagger\Client\PickwareDhlTrackingCodeApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPickwareDhlTrackingCode**](PickwareDhlTrackingCodeApi.md#createpickwaredhltrackingcode) | **POST** /pickware-dhl-tracking-code | Create a new Pickware Dhl Tracking Code resources
[**deletePickwareDhlTrackingCode**](PickwareDhlTrackingCodeApi.md#deletepickwaredhltrackingcode) | **DELETE** /pickware-dhl-tracking-code/{id} | Delete a Pickware Dhl Tracking Code resource
[**getPickwareDhlTrackingCode**](PickwareDhlTrackingCodeApi.md#getpickwaredhltrackingcode) | **GET** /pickware-dhl-tracking-code/{id} | Detailed information about a Pickware Dhl Tracking Code resource
[**getPickwareDhlTrackingCodeList**](PickwareDhlTrackingCodeApi.md#getpickwaredhltrackingcodelist) | **GET** /pickware-dhl-tracking-code | List with basic information of Pickware Dhl Tracking Code resources
[**updatePickwareDhlTrackingCode**](PickwareDhlTrackingCodeApi.md#updatepickwaredhltrackingcode) | **PATCH** /pickware-dhl-tracking-code/{id} | Partially update information about a Pickware Dhl Tracking Code resource

# **createPickwareDhlTrackingCode**
> \Swagger\Client\Model\InlineResponse200191 createPickwareDhlTrackingCode($body, $_response)

Create a new Pickware Dhl Tracking Code resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body129(); // \Swagger\Client\Model\Body129 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlTrackingCode($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->createPickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body129**](../Model/Body129.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200191**](../Model/InlineResponse200191.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPickwareDhlTrackingCode**
> \Swagger\Client\Model\InlineResponse200191 createPickwareDhlTrackingCode($body, $_response)

Create a new Pickware Dhl Tracking Code resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body129(); // \Swagger\Client\Model\Body129 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createPickwareDhlTrackingCode($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->createPickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body129**](../Model/Body129.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200191**](../Model/InlineResponse200191.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePickwareDhlTrackingCode**
> deletePickwareDhlTrackingCode($id, $_response)

Delete a Pickware Dhl Tracking Code resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_tracking_code
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deletePickwareDhlTrackingCode($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->deletePickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_tracking_code |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlTrackingCode**
> \Swagger\Client\Model\InlineResponse200191 getPickwareDhlTrackingCode($id)

Detailed information about a Pickware Dhl Tracking Code resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_tracking_code

try {
    $result = $apiInstance->getPickwareDhlTrackingCode($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->getPickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_tracking_code |

### Return type

[**\Swagger\Client\Model\InlineResponse200191**](../Model/InlineResponse200191.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPickwareDhlTrackingCodeList**
> \Swagger\Client\Model\InlineResponse200189 getPickwareDhlTrackingCodeList($limit, $page, $query)

List with basic information of Pickware Dhl Tracking Code resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getPickwareDhlTrackingCodeList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->getPickwareDhlTrackingCodeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200189**](../Model/InlineResponse200189.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlTrackingCode**
> \Swagger\Client\Model\InlineResponse200191 updatePickwareDhlTrackingCode($id, $body, $_response)

Partially update information about a Pickware Dhl Tracking Code resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_tracking_code
$body = new \Swagger\Client\Model\Body130(); // \Swagger\Client\Model\Body130 | Partially update information about a Pickware Dhl Tracking Code resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlTrackingCode($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->updatePickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_tracking_code |
 **body** | [**\Swagger\Client\Model\Body130**](../Model/Body130.md)| Partially update information about a Pickware Dhl Tracking Code resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200191**](../Model/InlineResponse200191.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePickwareDhlTrackingCode**
> \Swagger\Client\Model\InlineResponse200191 updatePickwareDhlTrackingCode($id, $body, $_response)

Partially update information about a Pickware Dhl Tracking Code resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PickwareDhlTrackingCodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the pickware_dhl_tracking_code
$body = new \Swagger\Client\Model\Body130(); // \Swagger\Client\Model\Body130 | Partially update information about a Pickware Dhl Tracking Code resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updatePickwareDhlTrackingCode($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PickwareDhlTrackingCodeApi->updatePickwareDhlTrackingCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the pickware_dhl_tracking_code |
 **body** | [**\Swagger\Client\Model\Body130**](../Model/Body130.md)| Partially update information about a Pickware Dhl Tracking Code resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200191**](../Model/InlineResponse200191.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

