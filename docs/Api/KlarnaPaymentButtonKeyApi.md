# Swagger\Client\KlarnaPaymentButtonKeyApi

All URIs are relative to *http://localhost/api/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createKlarnaPaymentButtonKey**](KlarnaPaymentButtonKeyApi.md#createklarnapaymentbuttonkey) | **POST** /klarna-payment-button-key | Create a new Klarna Payment Button Key resources
[**deleteKlarnaPaymentButtonKey**](KlarnaPaymentButtonKeyApi.md#deleteklarnapaymentbuttonkey) | **DELETE** /klarna-payment-button-key/{id} | Delete a Klarna Payment Button Key resource
[**getKlarnaPaymentButtonKey**](KlarnaPaymentButtonKeyApi.md#getklarnapaymentbuttonkey) | **GET** /klarna-payment-button-key/{id} | Detailed information about a Klarna Payment Button Key resource
[**getKlarnaPaymentButtonKeyList**](KlarnaPaymentButtonKeyApi.md#getklarnapaymentbuttonkeylist) | **GET** /klarna-payment-button-key | List with basic information of Klarna Payment Button Key resources
[**updateKlarnaPaymentButtonKey**](KlarnaPaymentButtonKeyApi.md#updateklarnapaymentbuttonkey) | **PATCH** /klarna-payment-button-key/{id} | Partially update information about a Klarna Payment Button Key resource

# **createKlarnaPaymentButtonKey**
> \Swagger\Client\Model\InlineResponse20092 createKlarnaPaymentButtonKey($body, $_response)

Create a new Klarna Payment Button Key resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body63(); // \Swagger\Client\Model\Body63 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createKlarnaPaymentButtonKey($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->createKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body63**](../Model/Body63.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createKlarnaPaymentButtonKey**
> \Swagger\Client\Model\InlineResponse20092 createKlarnaPaymentButtonKey($body, $_response)

Create a new Klarna Payment Button Key resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\Body63(); // \Swagger\Client\Model\Body63 | 
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->createKlarnaPaymentButtonKey($body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->createKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Body63**](../Model/Body63.md)|  | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteKlarnaPaymentButtonKey**
> deleteKlarnaPaymentButtonKey($id, $_response)

Delete a Klarna Payment Button Key resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_button_key
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $apiInstance->deleteKlarnaPaymentButtonKey($id, $_response);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->deleteKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_button_key |
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

void (empty response body)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKlarnaPaymentButtonKey**
> \Swagger\Client\Model\InlineResponse20092 getKlarnaPaymentButtonKey($id)

Detailed information about a Klarna Payment Button Key resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_button_key

try {
    $result = $apiInstance->getKlarnaPaymentButtonKey($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->getKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_button_key |

### Return type

[**\Swagger\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKlarnaPaymentButtonKeyList**
> \Swagger\Client\Model\InlineResponse20090 getKlarnaPaymentButtonKeyList($limit, $page, $query)

List with basic information of Klarna Payment Button Key resources

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 56; // int | Max amount of resources to be returned in a page
$page = 56; // int | The page to be returned
$query = "query_example"; // string | Encoded SwagQL in JSON

try {
    $result = $apiInstance->getKlarnaPaymentButtonKeyList($limit, $page, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->getKlarnaPaymentButtonKeyList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Max amount of resources to be returned in a page | [optional]
 **page** | **int**| The page to be returned | [optional]
 **query** | **string**| Encoded SwagQL in JSON | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20090**](../Model/InlineResponse20090.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKlarnaPaymentButtonKey**
> \Swagger\Client\Model\InlineResponse20092 updateKlarnaPaymentButtonKey($id, $body, $_response)

Partially update information about a Klarna Payment Button Key resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_button_key
$body = new \Swagger\Client\Model\Body64(); // \Swagger\Client\Model\Body64 | Partially update information about a Klarna Payment Button Key resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateKlarnaPaymentButtonKey($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->updateKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_button_key |
 **body** | [**\Swagger\Client\Model\Body64**](../Model/Body64.md)| Partially update information about a Klarna Payment Button Key resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKlarnaPaymentButtonKey**
> \Swagger\Client\Model\InlineResponse20092 updateKlarnaPaymentButtonKey($id, $body, $_response)

Partially update information about a Klarna Payment Button Key resource

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\KlarnaPaymentButtonKeyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "38400000-8cf0-11bd-b23e-10b96e4ef00d"; // string | Identifier for the klarna_payment_button_key
$body = new \Swagger\Client\Model\Body64(); // \Swagger\Client\Model\Body64 | Partially update information about a Klarna Payment Button Key resource.
$_response = "_response_example"; // string | Data format for response. Empty if none is provided.

try {
    $result = $apiInstance->updateKlarnaPaymentButtonKey($id, $body, $_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KlarnaPaymentButtonKeyApi->updateKlarnaPaymentButtonKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| Identifier for the klarna_payment_button_key |
 **body** | [**\Swagger\Client\Model\Body64**](../Model/Body64.md)| Partially update information about a Klarna Payment Button Key resource. | [optional]
 **_response** | **string**| Data format for response. Empty if none is provided. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[oAuth](../../README.md#oAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.api+json, application/json
 - **Accept**: application/vnd.api+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

